"""gifting URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from diwali import views 
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.iView),
    url(r'^gifting/', views.indexView),
    url(r'^contactus/', views.contactUs),  
    url(r'^weblog/', include('zinnia.urls')),
    url(r'^litwit/', include('mainApp.urls')),
    url(r'^comments/', include('django_comments.urls')),
    url(r'^tagging_autocomplete_tagit/', include('tagging_autocomplete_tagit.urls')),
    

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
