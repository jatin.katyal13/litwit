"""Views for Zinnia sitemap"""
from django.views.generic import TemplateView


class Contact(TemplateView):
    """
    Sitemap view of the Weblog.
    """
    template_name = 'zinnia/contactus.html'

    
