"""Views for Zinnia sitemap"""
from django.views.generic import TemplateView



class About(TemplateView):
    """
    Sitemap view of the Weblog.
    """
    template_name = 'zinnia/aboutus.html'
    

