"""Views for Zinnia comments"""
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponsePermanentRedirect
from django.template.defaultfilters import slugify
from django.views.generic.base import TemplateResponseMixin
from django.views.generic.base import View
from django.contrib.auth.decorators import login_required
import secretballot
from django.shortcuts import get_object_or_404,get_list_or_404
from zinnia.models.entry import Entry
from django.http import HttpResponseRedirect,JsonResponse,HttpResponse
from zinnia.models.likes import Likes
from django.db.models import Q ,F
from django.shortcuts import render
class likeup(TemplateResponseMixin, View):
    """
    View for handing the publication of a Comment on an Entry.
    Do a redirection if the comment is visible,
    else render a confirmation template.
    """
    def get(self, request, pk, format=None):
        return self.likeReview(request,pk)

    def likeReview(self,request,pk):    
     dictV={}
     book = Entry.objects.get(pk=pk)
     print request.user
     if not request.session.session_key:
        request.session.save()
     session_id = request.session.session_key
     print session_id
     alreadyLiked = Likes.objects.filter(session_id=session_id,entry=book)
     print alreadyLiked
     if not alreadyLiked:
        item=Likes.objects.create(entry=book,session_id=session_id)
        count=Entry.objects.filter(pk=pk).update(likes=F("likes") + 1)
     counter=book.likes   
     dictV['count']=counter
     print counter
        
     return JsonResponse(dictV)


class likecheck(TemplateResponseMixin, View):
    """
    View for handing the publication of a Comment on an Entry.
    Do a redirection if the comment is visible,
    else render a confirmation template.
    """
    def get(self, request,format=None):
        return self.checklike(request)

    def checklike(self,request):    
     dictV={}
     book = Entry.objects.all()
     #print request.user
     if not request.session.session_key:
        request.session.save()
     session_id = request.session.session_key
     #print session_id
     #alreadyLiked = Likes.objects.filter(session_id=session_id)
     #print alreadyLiked
     for b in book:
         try:
             alreadyLiked=Likes.objects.get(session_id=session_id, entry=b)
         except Likes.DoesNotExist:
             alreadyLiked=None
         if not alreadyLiked:
            dictV[b.pk]=1
         else:
            dictV[b.pk]=0
     print dictV
     return render(request,'zinnia/skeleton.html',dictV)

