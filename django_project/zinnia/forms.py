from django import forms
from django.contrib import auth
from models import *
from django.forms.extras.widgets import SelectDateWidget
from taggit.forms import TagWidget
from django_messages.models import Message
from taggit.managers import TaggableManager
from zinnia.models.getfeatured import UserEntry
class userentry(forms.ModelForm):
    title = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Name','class': 'form-control',}))
    category = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Pen Name','class': 'form-control',}))

    caption = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control','rows': '3','maxlength':'500'}),required=False)
    tags = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Phone Number','class': 'form-control',}))
    content = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Pen Name','class': 'form-control',}))

    class Meta:
        fields = ( 'title','category','caption','tags','content' )
        model = UserEntry

