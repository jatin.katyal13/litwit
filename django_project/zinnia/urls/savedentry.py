"""Urls for the Zinnia sitemap"""
from django.conf.urls import url

from zinnia.views.feature import savedfeature


urlpatterns = [
    url(r'^$', savedfeature,
        name='savedfeature'),
]
