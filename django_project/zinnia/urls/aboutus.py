"""Urls for the Zinnia sitemap"""
from django.conf.urls import url

from zinnia.views.aboutus import About


urlpatterns = [
    url(r'^$', About.as_view(),
        name='about'),
]
