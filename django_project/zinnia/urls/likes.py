"""Urls for the Zinnia comments"""
from django.conf.urls import url

from zinnia.urls import _
from zinnia.views.likes import likeup
from zinnia.views.likes import likecheck


urlpatterns = [
    url(_(r'^up/(?P<pk>\d*)/$'),
        likeup.as_view(),
        name='likeup'),

    url(_(r'^check/$'),
        likecheck.as_view(),
        name='likecheck'),
]
