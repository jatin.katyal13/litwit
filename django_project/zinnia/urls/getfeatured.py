"""Urls for the Zinnia sitemap"""
from django.conf.urls import url

from zinnia.views.feature import getfeatured


urlpatterns = [
    url(r'^$', getfeatured.as_view(),
        name='getfeatured'),
]
