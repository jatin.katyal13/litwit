"""Urls for the Zinnia sitemap"""
from django.conf.urls import url

from zinnia.views.contact import Contact


urlpatterns = [
    url(r'^$', Contact.as_view(),
        name='contact'),
]
