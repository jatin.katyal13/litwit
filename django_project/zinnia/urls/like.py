from zinnia.models import Entry
from django.conf.urls import url
from voting.views import vote_on_object

tip_dict = {
      'model': Entry,
      'template_object_name': 'entry',
      'slug_field': 'slug',
      'allow_xmlhttprequest': 'true',
  }

urlpatterns = [
     url(r'^(?P[-\w]+)/(?Pup|down|clear)vote/?$', vote_on_object, tip_dict, name="tip-voting"),
]


