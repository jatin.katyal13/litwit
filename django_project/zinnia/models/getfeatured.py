"""Base entry models for Zinnia"""
import os

from django.contrib.sites.models import Site
from django.db import models
from django.db.models import Q
from django.template.defaultfilters import slugify
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.html import strip_tags
from django.utils.text import Truncator
from django.utils.translation import ugettext_lazy as _

import django_comments as comments
from django_comments.models import CommentFlag

from tagging.fields import TagField
from tagging.utils import parse_tag_input

from zinnia.flags import PINGBACK
from zinnia.flags import TRACKBACK
from zinnia.managers import DRAFT, HIDDEN, PUBLISHED
from zinnia.managers import EntryPublishedManager
from zinnia.managers import entries_published
from zinnia.markups import html_format
from zinnia.preview import HTMLPreview
from zinnia.settings import AUTO_CLOSE_COMMENTS_AFTER
from zinnia.settings import AUTO_CLOSE_PINGBACKS_AFTER
from zinnia.settings import AUTO_CLOSE_TRACKBACKS_AFTER
from zinnia.settings import ENTRY_CONTENT_TEMPLATES
from zinnia.settings import ENTRY_DETAIL_TEMPLATES
from zinnia.settings import UPLOAD_TO
from zinnia.url_shortener import get_url_shortener

#import secretballot

#from mainApp.models import BookedUser

class UserEntry(models.Model):
    """
    Abstract core entry model class providing
    the fields and methods required for publishing
    content over time.
    """
   
    title = models.CharField(
        _('title'), max_length=255)

    category = models.CharField(
        _('category'), max_length=255)

    caption = models.CharField(
        _('caption'), max_length=255)

    tags = models.CharField(
        _('tags'), max_length=255)

    content = models.TextField(_('content'), blank=True)
    @property
    def html_content(self):
        """
        Returns the "content" field formatted in HTML.
        """
        return html_format(self.content)
    

