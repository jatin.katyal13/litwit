"""Admin of Zinnia"""
from django.contrib import admin

from zinnia.admin.category import CategoryAdmin
from zinnia.admin.entry import EntryAdmin
from zinnia.models.category import Category
from zinnia.models.entry import Entry
from zinnia.models.author import Author
from zinnia.models.likes import Likes
from zinnia.models.getfeatured import UserEntry
from zinnia.settings import ENTRY_BASE_MODEL
from zinnia.models.contactus import ContactUs

if ENTRY_BASE_MODEL == 'zinnia.models_bases.entry.AbstractEntry':
    admin.site.register(Entry, EntryAdmin)

admin.site.register(Category, CategoryAdmin)
admin.site.register(Author)
admin.site.register(Likes)
admin.site.register(UserEntry)
admin.site.register(ContactUs)
