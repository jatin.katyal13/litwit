#Store book data from csv into postgresql
import psycopg2
conn = psycopg2.connect(host='localhost',user='postgres',password='sun123',database='booked',port=5433)
#conn = psycopg2.connect("dbname=booked user=root password=root")
cur = conn.cursor()


x = open('booksData.csv','r')
data = x.read()

dataList = []

for row in data.split('\n'):
    dictV = {}
    print row
    fields  = row.split('*')
    try:
            dictV['isbn']  = fields[0]
            dictV['title'] =fields[1]
            dictV['author'] =fields[2]
            dictV['edition'] =fields[3]
            dictV['rating'] =fields[4]
            if not dictV['rating']:
                dictV['rating'] = 3
            dictV['genre'] =fields[5]
            dictV['numPages'] =fields[6]
            if not dictV['numPages']:
                dictV['numPages'] = 0
            dictV['price'] =fields[7]
            if not dictV['price']:
                dictV['price'] = 0
            dataList.append(dictV)

    except IndexError:
        print str(IndexError)

print dataList

for row in dataList:
    row['title'] = row['title'].replace("'", r"")
    row['author'] = row['author'].replace("'", r"")
    query = """INSERT INTO
        "mainApp_book" ("isbn","title","author","edition","bookType","rating","genre","numPages","cost","inStock","numOfCopies","refundableSecurity","rent")
	VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%r','%d','%d','%d','%s')
	"""%(row['isbn'],row['title'],row['author'],row['edition'],row['edition'],row['rating'],row['genre'],row['numPages'],row['price'],False,100,0,0)
    cur.execute(query)

conn.commit()
