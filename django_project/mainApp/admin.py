from django.contrib import admin
from models import *
# Register your models here.
admin.site.register(BookedUser)
admin.site.register(Book)
admin.site.register(Exchange)
admin.site.register(Subscriptionlist)
admin.site.register(CoverPhoto)
admin.site.register(Rental)
admin.site.register(Shipment)
admin.site.register(UserAddress)
admin.site.register(Purchases)
admin.site.register(SeriesPurchases)
admin.site.register(readBooks)
admin.site.register(borrowedBooks)
admin.site.register(possessedBooks)
admin.site.register(CartBook)
admin.site.register(CartSeries)
admin.site.register(RentItem)
admin.site.register(todayBook)
admin.site.register(bestSellers)
admin.site.register(ContactUs)
admin.site.register(wishlist)
admin.site.register(opinion)
admin.site.register(Reviewlike)
admin.site.register(Discuss)
admin.site.register(Tag)
admin.site.register(Subscription)
admin.site.register(Follow)
admin.site.register(SubscriptionPlan)
admin.site.register(SubscriptionPurchase)
admin.site.register(Activity)
admin.site.register(DiscussionComment)
admin.site.register(ReplyDcomment)
admin.site.register(Series)
admin.site.register(SeriesPhoto)
admin.site.register(Badge)
admin.site.register(popularAuthor)
