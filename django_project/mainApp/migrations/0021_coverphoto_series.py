# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-07-04 05:26
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mainApp', '0020_auto_20170703_1051'),
    ]

    operations = [
        migrations.AddField(
            model_name='coverphoto',
            name='series',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='mainApp.Series'),
        ),
    ]
