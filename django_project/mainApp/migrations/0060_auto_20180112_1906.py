# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-01-12 19:06
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mainApp', '0059_auto_20180112_1856'),
    ]

    operations = [
        migrations.DeleteModel(
            name='CartItem',
        ),
    ]
