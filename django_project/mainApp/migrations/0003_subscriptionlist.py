# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-06-22 19:49
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mainApp', '0002_subscription'),
    ]

    operations = [
        migrations.CreateModel(
            name='subscriptionlist',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('booktype', models.IntegerField(default=0)),
                ('order', models.IntegerField(default=0)),
                ('delievered', models.BooleanField(default=False)),
                ('returned', models.BooleanField(default=False)),
                ('book', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='booksubscribe', to='mainApp.Book')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='subscribelists', to='mainApp.BookedUser')),
            ],
        ),
    ]
