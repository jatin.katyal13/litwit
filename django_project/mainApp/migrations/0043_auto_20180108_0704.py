# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-01-08 07:04
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('mainApp', '0042_auto_20180108_0659'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cartitem',
            name='book',
        ),
        migrations.AddField(
            model_name='cartitem',
            name='item',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='define_cart_object', to='contenttypes.ContentType'),
        ),
        migrations.AddField(
            model_name='cartitem',
            name='item_object_id',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
