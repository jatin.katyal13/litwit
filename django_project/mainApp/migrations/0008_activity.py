# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-06-30 06:34
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('mainApp', '0007_remove_bookeduser_follows'),
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('verb', models.TextField(blank=True, null=True)),
                ('action_object_object_id', models.CharField(blank=True, max_length=255, null=True)),
                ('visibility', models.CharField(choices=[('Everyone', 'Everyone'), ('Followers', 'Followers'), ('Private', 'Private')], default='Everyone', max_length=20)),
                ('action_object_content_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='define_action_object', to='contenttypes.ContentType')),
                ('actor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='useractor', to='mainApp.BookedUser')),
                ('target', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='targetuser', to='mainApp.BookedUser')),
            ],
        ),
    ]
