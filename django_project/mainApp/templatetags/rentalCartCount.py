from django import template
from mainApp.models import RentItem,BookedUser

register = template.Library()

@register.filter(name='rentalCartNum')
def rentalCartNum(user):
    #print user
    try:
        userObj = BookedUser.objects.get(authUser=user)
    except BookedUser.DoesNotExist:
        return 0
    count = RentItem.objects.filter(user=userObj).count()
    return count
