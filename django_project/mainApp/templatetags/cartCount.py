from django import template
from mainApp.models import CartBook,CartSeries,BookedUser,RentItem

register = template.Library()

#NOTE: Returns the total items in cart(buy,rent both)
@register.filter(name='cartNum')
def cartNum(user):
    count = 0
    try:
        userObj = BookedUser.objects.get(authUser=user)
    except BookedUser.DoesNotExist:
        return 0
    count = CartBook.objects.filter(user=userObj).count()
    count = CartSeries.objects.filter(user=userObj).count()
    count += RentItem.objects.filter(user=userObj).count()
    return count
