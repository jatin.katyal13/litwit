from django import template
from mainApp.models import BookedUser, Follow

register = template.Library()

@register.filter(name='valueAt')
def cartNum(value,index):
    try:
        return value[index]
    except:
        return None

@register.filter
def get_item(dictionary,opinions):
    return dictionary.num_like


@register.simple_tag
def get_Status(a, b):
    for q in a:
       if q.follower.pk==b.pk:
          return 1
    return 0


@register.simple_tag
def get_Like(user, likes):
    for like in likes:
       if like.user==user:
          return 1
    return 0
