from django import template
from mainApp.models import opinion, Reviewlike
from django.shortcuts import get_object_or_404,get_list_or_404
register = template.Library()

@register.simple_tag(name='get_ls')
def get_Like(user,opk):
    review = get_object_or_404(opinion, pk=opk)
    userlist=Reviewlike.objects.filter(opinions=review)
    for like in userlist:
       if like.user.authUser==user:
          return 0,userlist
    return 1,userlist
