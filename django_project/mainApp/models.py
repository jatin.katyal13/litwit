from __future__ import unicode_literals
from django.db import models
from django.contrib import auth
from django.core.validators import MinValueValidator
from django.utils import timezone
from django.utils.encoding import force_unicode
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from taggit.managers import TaggableManager
from tagging_autocomplete_tagit.models import TagAutocompleteTagItField
from .FTPStorage import FTPStorage
fs=FTPStorage()
paymentChoices = ( ('COD','Cash On Delivery'), ('Online','Online') ) #Online means via razorpay (debit card etc)
conditionChoices = ( ('As New','As New'),('Fine','Fine'),('Very Good','Very Good'),('Good','Good'),('Fair','Fair'),('Poor','Poor'),('Binding Copy','Binding Copy'),('Reading Copy','Reading Copy'), )
bookTypeChoices = ( ('Paperback','Paperback'),('Hardcover','Hardcover'), )
postvisibilityChoices=(('Everyone','Everyone'),('Followers','Followers'),('Private','Private'))
def currentDate():
    return timezone.now().date()

def currentDateTime():
    return timezone.now()

#tags
class Tag(models.Model):
	slug =models.SlugField(max_length=200,unique=True)
	def __str__(self):
		return self.slug


#Active if all details are filled and otp has been verified
class BookedUser(models.Model):
    name = models.CharField(max_length = 100,blank=True,null=True)
    dob = models.DateField(blank=True,null=True,default=None) #Set to current time ONLY WHEN OBJECT IS CREATED
    address = models.CharField(max_length=500,blank=True,null=True)
    latitude=models.DecimalField(max_digits=9, decimal_places=6,null=True)
    longitude=models.DecimalField(max_digits=9, decimal_places=6,null=True)
    phone = models.CharField(max_length = 12, blank=True,null = True)
    penName = models.CharField(max_length = 100,blank=True,null=True)
    authUser = models.OneToOneField(auth.models.User)
    otp = models.CharField(max_length = 6,default='',blank=True,null=True)
    canBorrow = models.BooleanField(default=False) #users will have to verify via otp and then they can borrow or purchase from us
    profilePic = models.FileField(storage=fs,null=True)
    coverPic = models.ImageField(blank=True,null=True)
    favgenres = TagAutocompleteTagItField(max_tags=False)
    favauthors = TagAutocompleteTagItField(max_tags=False)
    isSubscribed=models.BooleanField(default=False)
    def __unicode__(self):
        try:
            n = str(self.name.encode('utf-8').strip())
        except:
            n = 'user'
        try:
            p = str(self.penName.encode('utf-8').strip())
        except:
            p = 'penname'
        return force_unicode(n +' aka '+ p)

class Book(models.Model):
    #Cover Photos : via foreign key
    isbn = models.CharField(max_length = 18)
    title = models.CharField(max_length = 300)
    author = models.CharField(max_length = 300)
    edition = models.CharField(max_length = 50)
    releaseDate = models.DateField(default=currentDate,blank=True,null=True) #Set to current time ONLY WHEN OBJECT IS CREATED
    rating = models.FloatField( validators=[MinValueValidator(0)],default=3 )
    genre = models.CharField(max_length = 300) #comma separated values
    numPages = models.IntegerField(default=1)
    inStock = models.BooleanField(default=False) #Indicating whether the book is in booked library or not
    numOfCopies = models.IntegerField(default=0)
    cost = models.FloatField( validators=[MinValueValidator(1)],default=100 ) #Cost price of book
    discount = models.FloatField( validators=[MinValueValidator(1)],default=30 ) #percentange of discount given on a book
    #selling price = (100-discount)*cost / 100
    description = models.CharField(max_length = 1000,blank=True,null=True)
    bookType = models.CharField(choices = bookTypeChoices, default = 'Paperback', max_length=20) #Academic or Novels etc
    boolmovie=models.BooleanField(default=True)
    moviename=models.CharField(max_length = 300,blank=True)
    movierating=models.IntegerField(blank= True, null=True)
    moviereleaseDate=models.DateField(blank=True,null=True)
    moviecertificate=models.CharField(blank=True,max_length=10)
    #TO-DO: movie cast
    #Details for renting (total cost is security + rent)
    refundableSecurity = models.FloatField( validators=[MinValueValidator(1)] ) #refunadable amount
    rent = models.FloatField( validators=[MinValueValidator(1)] ) #rent calculated acc to cost of book and number of days
    series=models.IntegerField(default=0)
    #discussion=GenericRelation('Discuss', related_query_name = 'book')
    #Exchanges: via foreign key
    def __unicode__(self):
        n = self.title
        p = self.author
        return force_unicode(n +' by '+ p)

#Series Model
class Series(models.Model):
    seriesNo=models.IntegerField(default=0)
    seriesName=models.CharField(max_length = 300,blank=True)
    description = models.CharField(max_length = 1000,blank=True,null=True)

    seriesAuthor = models.CharField(max_length = 300, blank = True)
    seriesRating = models.FloatField( validators=[MinValueValidator(0)],default=3 )
    cost = models.FloatField( validators=[MinValueValidator(1)],default=100 ) #Cost price of book
    discount = models.FloatField( validators=[MinValueValidator(1)],default=30 ) #percentange of discount given on a book
    def __unicode__(self):
        n = self.seriesNo
        p = self.seriesName
        return force_unicode(p +' Series ')

#Multiple cover photos of a series are allowed
class SeriesPhoto(models.Model):
    series = models.ForeignKey(Series)

    photo = models.FileField(storage=fs,null=True)
    def __unicode__(self):
        return self.series.seriesName


#Exchange between books of two different users or lending of a book from one user to another
class Exchange(models.Model):
    user1 = models.ForeignKey(BookedUser,related_name='fromUser') #who initiated the exchange
    book1 = models.ForeignKey(Book,related_name='book1',blank=True,null=True) #book possessed by the user1 (optional)
    user2 = models.ForeignKey(BookedUser,related_name='toUser') #person with whom he wants to exchange
    book2 = models.ForeignKey(Book,related_name='book2') #book that he wants to exchange
    approved = models.BooleanField(default=False) #approved by user2 or not
    returned = models.BooleanField(default=False) #returned the book or not (user2)
    returnVerified = models.BooleanField(default=False) #Verify whether books were actually returned (user1)
    disapproved = models.BooleanField(default = False) #If the user2 disapproved the exchange, do not show it to him/her again
    message = models.CharField(max_length = 1000,blank=True,default=True) #Message by user1
    #toUser = models.ForeignKey(BookedUser,related_name='toUser')
    def __unicode__(self):
        if self.book1:
            b1 = self.book1.title
        else:
            b1 = ''
        b2 = self.book2.title
        u1 = str(self.user1.name)
        u2 = str(self.user2.name)

        if self.book1:
            return b1 + ' and '+ b2 + ' exchanged between ' + u1 + ' and '+ u2
        else:
            return b2 + ' borrowed by ' + u1 + ' from '+ u2

#A user may review a book

#Multiple cover photos of a book are allowed
class CoverPhoto(models.Model):
    book = models.ForeignKey(Book)

    photo = models.FileField(storage = fs, null=True)
    def __unicode__(self):
        return self.book.title
class UserAddress(models.Model):
      user = models.ForeignKey(BookedUser, null=True)
      name = models.CharField(max_length = 100,blank=True,null=True)
      phone = models.CharField(max_length = 12, blank=True,null = True)  
      pincode = models.CharField(max_length = 12, blank=True,null = True)
      locality = models.CharField(max_length = 100,blank=True,null=True)
      streetAddress = models.CharField(max_length = 100,blank=True,null=True)
      city = models.CharField(max_length = 100,blank=True,null=True)
      state = models.CharField(max_length = 100,blank=True,null=True)
      landmark = models.CharField(max_length = 100,blank=True,null=True)
      addtype=models.CharField(max_length = 12, blank=True,null = True)

#One shipment contains many purchases or rentals
class Shipment(models.Model):
    createdOn = models.DateTimeField(default=currentDateTime) #Set to current time ONLY WHEN OBJECT IS CREATED
    razorpayId = models.CharField(max_length = 50,blank=True,null=True) #Razorpay payment id of the purchase (COD<pk> (ex. COD12) if COD is chosen)
    shipped = models.BooleanField(default=False)
    paymentMade = models.BooleanField(default=False) #True after user has paid via razorpay (or manually set it for COD)
    shippingAddress = models.ForeignKey(UserAddress,null=True)
    paymentOption = models.CharField(max_length=50,choices=paymentChoices)
    user = models.ForeignKey(BookedUser)
    totalCost = models.FloatField( validators=[MinValueValidator(1)] ) #Sum of all costs of purchases
    refundableAmount = models.FloatField( validators=[MinValueValidator(0)],default=0 ) #Total amount to be refunded (according to number of books and number of days)
    transactionComplete = models.BooleanField(default=False)
    #shipmentType = models.CharField( max_length=8,choices = shipmentTypeChoices,default='Purchase' )
    def __unicode__(self):
        return self.user.name + ' purchased on '+ str(self.createdOn)

#Purchased books (sold by litwit library)
class Purchases(models.Model):
    book = models.ForeignKey(Book)
    cost = models.FloatField( validators=[MinValueValidator(1)] )
    shipment = models.ForeignKey(Shipment) #Which shipment it belongs to
    class Meta:
        verbose_name = 'Purchase' #Name to be shown in django admin
    def __unicode__(self):
        return self.book.title + ' purchased by '+self.shipment.user.name

class SeriesPurchases(models.Model):
    series = models.ForeignKey(Series)
    cost = models.FloatField( validators=[MinValueValidator(1)] )
    shipment = models.ForeignKey(Shipment) #Which shipment it belongs to
    class Meta:
        verbose_name = 'Series Purchase' #Name to be shown in django admin
    def __unicode__(self):
        return self.series.seriesName + ' purchased by '+self.shipment.user.name


#Rented books (sold by litwit library)
class Rental(models.Model):
    book = models.ForeignKey(Book)
    shipment = models.ForeignKey(Shipment) #Which shipment it belongs to
    startDate = models.DateField(default=currentDate)
    duration = models.IntegerField(default = 7) #number of days for which the book is to be rented (3-60 days)
    returned = models.BooleanField(default=False)
    class Meta:
        verbose_name = 'Rental' #Name to be shown in django admin
    def __unicode__(self):
        return self.book.title + ' rented by '+self.shipment.user.name

#To mark a book as read by a user
class readBooks(models.Model):
    book = models.ForeignKey(Book)
    user = models.ForeignKey(BookedUser)
    class Meta:
        verbose_name = 'Read Book' #Name to be shown in django admin
    def __unicode__(self):
        return self.book.title + ' read by '+self.user.name

#To mark a book as borrowed by a user
class borrowedBooks(models.Model):
    book = models.ForeignKey(Book)
    user = models.ForeignKey(BookedUser)
    returned = models.BooleanField(default=False) #To indicate whether they have returned the book or not
    class Meta:
        verbose_name = 'Borrowed Book' #Name to be shown in django admin
    def __unicode__(self):
        return self.book.title + ' borrowed by '+self.user.name

#A person's own books that he/she wants to lend
class possessedBooks(models.Model):
    book = models.ForeignKey(Book)
    user = models.ForeignKey(BookedUser)
    condition = models.CharField(choices = conditionChoices, default = 'Good', max_length = 20)
    exchangedCurrently = models.BooleanField(default=False)
    #wantToLend = models.BooleanField(default=True) #check whether user wants to exchange it or not
    class Meta:
        verbose_name = 'Possessed Book' #Name to be shown in django admin
    def __unicode__(self):
        n = self.book.title
        if not n:
            n='unknown'
        p = self.user.name
        if not p:
            p='unknown'
        return n + ' possessed by '+p

#Cart Items (to be bought)
class CartBook(models.Model):
    book = models.ForeignKey(Book)
    user = models.ForeignKey(BookedUser)
    qty=models.IntegerField(default=1)
class CartSeries(models.Model):
    book = models.ForeignKey(Series)
    user = models.ForeignKey(BookedUser)
    qty=models.IntegerField(default=1)
#Rental Items (to be rented)
class RentItem(models.Model):
    book = models.ForeignKey(Book)
    user = models.ForeignKey(BookedUser)

#Popular Authors
class popularAuthor(models.Model):
    name = models.CharField(max_length = 300)
    genre = models.CharField(max_length = 300) # comma separated values
    image = models.FileField(storage = fs,null = True)
    def __unicode__(self):
        return self.author.name

#Latest books (to be added manually and shown in our library)
class todayBook(models.Model):
    book = models.ForeignKey(Book)
    def __unicode__(self):
        n = self.book.title
        if not n:
            n = 'unknown'
        p = self.book.title
        if not p:
            p = 'unknown'
        return n + ' by '+ p
    class Meta:
        verbose_name = 'Latest Books/Today Book' #Name to be shown in django admin

#Bestseller books (to be added manually and shown in our library)
class bestSellers(models.Model):
    book = models.ForeignKey(Book)
    def __unicode__(self):
        n = self.book.title
        if not n:
            n = 'unknown'
        p = self.book.title
        if not p:
            p = 'unknown'
        return n + ' by '+ p
    class Meta:
        verbose_name = 'Best Seller' #Name to be shown in django admin


#Contact Us Details
class ContactUs(models.Model):
    phone = models.IntegerField(blank=True,null=True)
    email = models.EmailField(blank=True,null=True)
    query = models.CharField(max_length = 5000)
    class Meta:
        verbose_name_plural = 'Queries (Contact Us)' #Name to be shown in django admin



class opinion(models.Model):
    RATING_CHOICES = (
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
    )
    book = models.ForeignKey('Book',related_name="opinions")
    pub_date = models.DateTimeField(blank=True, null=True)
    user_name = models.ForeignKey('BookedUser',related_name='useropinion')
    comment = models.TextField(default="Review")
    rating = models.IntegerField(choices=RATING_CHOICES, default="3")
    likecount=models.IntegerField(default=0)
    language=models.TextField(null=True,blank=True)
    writing=models.TextField(null=True,blank=True)
    cover=models.TextField(null=True,blank=True)
    plot=models.TextField(null=True,blank=True)
    lrating = models.IntegerField(choices=RATING_CHOICES,blank=True,null=True)
    crating = models.IntegerField(choices=RATING_CHOICES,blank=True,null=True)
    prating = models.IntegerField(choices=RATING_CHOICES,blank=True,null=True)
    wrating=models.IntegerField(choices=RATING_CHOICES,blank=True,null=True)
    def get_absolute_url(self):
            return "/opinion/%i/" % self.id

class wishlist(models.Model):


    item = models.ForeignKey(ContentType, blank=True, null=True,
                                                   related_name='define_wishlist_object')
    wishlist_object_id = models.CharField(max_length=255, blank=True, null=True)
    item_object = GenericForeignKey('item', 'wishlist_object_id')
      
    user = models.ForeignKey(BookedUser)


class Reviewlike(models.Model):
	user=models.ForeignKey('BookedUser',related_name='reviewLikes')
        opinions=models.ForeignKey('opinion',related_name='opinionLikes')
        

class Discusslike(models.Model):
	user=models.ForeignKey('BookedUser',related_name='discussLikes')
        discuss=models.ForeignKey('Discuss',related_name='discussLikes')

class Discuss(models.Model):
	user=models.ForeignKey('BookedUser',related_name='discussUser')
        pub_date = models.DateTimeField(blank=True, null=True)
        book = models.ForeignKey(ContentType, blank=True, null=True,
                                                   related_name='define_book_object')
        book_object_id = models.CharField(max_length=255, blank=True, null=True)
        book_object = GenericForeignKey('book', 'book_object_id')
      
        likecount=models.IntegerField(default=0)
        title=models.TextField()
        comment=models.TextField(null=True,blank=True)
        tags = TaggableManager()
        def get_absolute_url(self):
            return "/discuss/%i/" % self.id

class DiscussionComment(models.Model):
      discuss=models.ForeignKey('Discuss',related_name='discusscomment')	
      pub_date = models.DateTimeField(default=currentDateTime, null=True)
      user_name = models.ForeignKey('BookedUser',related_name='userdiscussion')
      likecount=models.IntegerField(default=0)
      comment=models.TextField(null=True,blank=True)
      def get_absolute_url(self):
            return "/discussComment/%i/" % self.id
  
class ReplyDcomment(models.Model):
      reply=models.ForeignKey('DiscussionComment',related_name='discussreply')	
      pub_date = models.DateTimeField(default=currentDateTime, null=True)
      user_name = models.ForeignKey('BookedUser',related_name='userdiscussionreply')
      likecount=models.IntegerField(default=0)
      comment=models.TextField(null=True,blank=True)
      def get_absolute_url(self):
            return "/reply/%i/" % self.id


class Subscription(models.Model):
    user = models.ForeignKey('BookedUser',related_name='subs')
    plan=models.ForeignKey('SubscriptionPlan')
    shipment = models.ForeignKey('Shipment',related_name='purchasesubs') #Which shipment it belongs to
    
    class Meta:
        verbose_name = 'Subscription' #Name to be shown in django admin
    def __unicode__(self):
        return self.user.name + ' Subscribed to  '+ str (self.plan )
class SubscriptionPlan(models.Model):
      name=models.CharField(max_length = 5000)
      duration=models.IntegerField(default=0)
      books=models.IntegerField(default=0)
      amount=models.IntegerField(default=300)
class Subscriptionlist(models.Model):
	user=models.ForeignKey('BookedUser',related_name='subscribelists')
	book=models.ForeignKey('Book',related_name='booksubscribe')
        booktype=models.IntegerField(default=0)
        order=models.IntegerField(default=0)
        delievered=models.BooleanField(default=False)
        returned=models.BooleanField(default=False)
class SubscriptionPurchase(models.Model):
    user = models.ForeignKey('BookedUser',related_name='subuser')
    book=models.ForeignKey('Book',related_name='bookslist')
    shipment = models.ForeignKey('Shipment',related_name='shipmentsubscription') #Which shipment it belongs to
    returned=models.BooleanField(default=False)  
    activity_time = models.DateTimeField(default=currentDateTime)
    createdOn = models.DateTimeField(default=currentDateTime)
    class Meta:
        verbose_name = 'SubscriptionPurchase' #Name to be shown in django admin
    def __unicode__(self):
        return self.user.name + ' Subscribed to  '+ str (self.shipment )

class Follow(models.Model):
      following = models.ForeignKey('BookedUser', related_name="who_follows")
      follower = models.ForeignKey('BookedUser', related_name="who_is_followed")
      follow_time = models.DateTimeField(auto_now=True)

      def __unicode__(self):
          return str(self.follow_time)


class Activity(models.Model):
      actor=models.ForeignKey('BookedUser',related_name="useractor")
      verb=models.TextField(null=True, blank=True)
      target=models.ForeignKey('BookedUser',related_name="targetuser", blank=True, null=True)
      action_object_content_type = models.ForeignKey(ContentType, blank=True, null=True,
                                                   related_name='define_action_object')
      action_object_object_id = models.CharField(max_length=255, blank=True, null=True)
      action_object = GenericForeignKey('action_object_content_type', 'action_object_object_id')
      visibility = models.CharField(choices = postvisibilityChoices, default = 'Everyone', max_length=20) 
      activity_time = models.DateTimeField(default=currentDateTime)

class Badge(models.Model):
      user=models.ForeignKey('BookedUser',related_name='userbadges')
      b1=models.BooleanField(default=False)  
      b2=models.BooleanField(default=False)
      b3=models.BooleanField(default=False)
      b4=models.BooleanField(default=False)
      b5=models.BooleanField(default=False)
      b6=models.BooleanField(default=False)
      b7=models.BooleanField(default=False)
      b8=models.BooleanField(default=False)
      b9=models.BooleanField(default=False)
      b10=models.BooleanField(default=False)
      b11=models.BooleanField(default=False)
      b12=models.BooleanField(default=False)
      b13=models.BooleanField(default=False)
      b14=models.BooleanField(default=False)
      b15=models.BooleanField(default=False)
      b16=models.BooleanField(default=False)
      b17=models.BooleanField(default=False)
      b18=models.BooleanField(default=False)
      b19=models.BooleanField(default=False)
      b20=models.BooleanField(default=False)
      
