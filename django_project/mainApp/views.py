from django.shortcuts import render
from django.contrib import auth
from forms import *
from django.contrib.auth.decorators import login_required
from models import *
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect,JsonResponse,HttpResponse
import razorpay
import uuid
from django.views.decorators.csrf import csrf_exempt
from django.template.context_processors import csrf
from django.contrib import auth
from django.db.models import Q ,F
import urllib,urllib2
from django.core.mail import EmailMessage
from django.utils import timezone
from django.shortcuts import get_object_or_404,get_list_or_404
from allauth.socialaccount.models import SocialAccount
from django.db.models import Count
from django.core import serializers
from django_messages.models import *
import datetime
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from notifications.signals import notify
from django.shortcuts import redirect
from django.core.exceptions import PermissionDenied

#razorpay settings
#TODO: Change this
client = razorpay.Client(auth=("rzp_test_7Kw2wESOi945RD", "iIgAzpFOChBa2pM4BtZWQ8nW")) #For Testing

conditionChoices = ( ('As New','As New'),('Fine','Fine'),('Very Good','Very Good'),('Good','Good'),('Fair','Fair'),('Poor','Poor'),('Binding Copy','Binding Copy'),('Reading Copy','Reading Copy'), )

#Send email to an id
def sendEmail(emailId,subject,content):
    sender_email = 'admin@book-ed.in'
    msg = EmailMessage(subject,content,sender_email,[emailId,])
    msg.content_subtype = "html"
    if emailId:
        try:
            msg.send()
        except Exception as e:
            print str(e)
        print "DONE"

#Send sms to a number
def sendSms(numbers,content):
    #numbers is a list of mobile numbers
    authKey = '101913Ak6Ehn1dRZZ568d56a3' #Testing
    authKey = '137395AmakpO5hnXNy587bbea0' #Live
    sender = 'LITWIT'
    route = '4'
    #print "NUMBERS:",numbers
    newNumList = []
    for num in numbers:
        newNumList.append(num[-10:])
    mobiles = (',').join(newNumList)
    #print "MOBILES:",mobiles
    values = {
              'authkey' : authKey,
              'mobiles' : mobiles,
              'message' : content,
              'sender' : sender,
              'route' : route
              }
    url = "https://control.msg91.com/api/sendhttp.php" # API URL
    postdata = urllib.urlencode(values) # URL encoding the data here.
    req = urllib2.Request(url, postdata)
    response = urllib2.urlopen(req)
    output = response.read()
    print output #Print Response

#Homepage
def indexView(request):
    #Popular Authors
    #Filterable using get parameters
    pAuthors = popularAuthor.objects.all()
    if request.GET.get("genre"):
        genre = request.GET.get("genre")    
        pAuthors = pAuthors.filter(genre__icontains = genre)
    pAuthors = pAuthors[:12]

    #Today's Books (can be changed to classic reads)
    #Filterable using get parameters
    qry = todayBook.objects.all().order_by('-pk')
    if request.GET.get("genre"):
        genre = request.GET.get("genre")
        qry = qry.filter(book__genre__icontains = genre)
    if request.GET.get("author"):
        author = request.GET.get("author")
        qry = qry.filter(book__author__icontains = author)
    qry = qry[:10]
    today_books = list()
    for b in qry:
        try:
            img = CoverPhoto.objects.filter(book=b.book)[0] #Get the first cover photo of the image
        except:
            img = ''
        today_books.append((b.book, img,(100-b.book.discount)*b.book.cost/100))

    #Manually added bestsellers
    #Filterable using get parameters
    qry = bestSellers.objects.all().order_by('-pk')
    if request.GET.get("genre"):
        genre = request.GET.get("genre")
        qry = qry.filter(book__genre__icontains = genre)
    if request.GET.get("author"):
        author = request.GET.get("author")
        qry = qry.filter(book__author__icontains = author)
    qry = qry[:10]
    best_sellers = list()
    for b in qry:
        try:
            img = CoverPhoto.objects.filter(book=b.book)[0].photo #Get the first cover photo of the image
            img1="/".join(str(img).split('/')[1:])

        except:
            img1 = ''
        best_sellers.append((b.book, img1, (100-b.book.discount)*b.book.cost/100))
        img = CoverPhoto.objects.filter(book=b.book)[0].photo #Get the first cover photo of the image




    series = None

    series_list=list()


    if not request.GET.get("genre") or not request.GET.get("author"):

        #remove when get filers
        sqry = Series.objects.all().order_by('-pk')[:10]
        for s in sqry:
         try:
            simg = SeriesPhoto.objects.filter(series=s)[0].photo #Get the first cover photo of the image
            img1="/".join(str(simg).split('/')[1:])

         except:
            img1 = ''
         series_list.append((s, img1))
         print "the cover photo is"
         print img1
    #Latest Books
    #filterable using get parameters
    latestBooks = Book.objects.all().order_by('-releaseDate') #10 latest books
    if request.GET.get("genre"):
        genre = request.GET.get("genre")

        latestBooks.filter(genre__icontains = genre)

        latestBooks = latestBooks.filter(genre__icontains = genre)



    if request.GET.get("author"):
        author = request.GET.get("author")
        latestBooks = latestBooks.filter(author__icontains = author)

    latestBooks = latestBooks[:10]
    latest_books = list()
    for b in latestBooks:
        try:
            img = CoverPhoto.objects.filter(book=b)[0] #Get the first cover photo of the image
            img1="/".join(str(img).split('/')[1:])
        except:
            img = ''
        latest_books.append((b, img, (100-b.discount)*b.cost / 100))
    
    context = {
        "series" : series_list,
        "popularAuthors" : pAuthors,
        "todayBooks" : today_books,
        "bestSellers" : best_sellers,
        "latestBooks" : latest_books,
        "form" : loginForm()
    }

    return render(request,'ourLibrary.html',context)

#About Us
def aboutUs(request):
    dictV = {}
    return render(request,'aboutUs.html',dictV)

#Privacy Policy
def privacyPolicy(request):
    dictV = {}
    return render(request,'privacyPolicy.html',dictV)

#Frequently Asked Questions
def faqs(request):
    dictV = {}
    return render(request,'faqs.html',dictV)

#Terms and Conditions
def tnc(request):
    dictV = {}
    return render(request,'tnc.html',dictV)

#Refund Policy
def refundPolicy(request):
    dictV = {}
    return render(request,'refundPolicy.html',dictV)

#Contact Us
def contactUs(request):
    dictV = {}
    dictV['form'] = contactForm()
    if request.method == 'POST':
        form = contactForm(request.POST)
        if form.is_valid():
            form.save()
            dictV['success'] = True
        else:
            dictV['error'] = True
    return render(request,'contactUs.html',dictV)

#Return (page number num) of paginated objList
def paginateObjects(objList,num):
    p = Paginator(objList,10)
    if num > 0:
        try:
            num = int(num)
        except:
            num = 1
    else:
        num = 1
    #print p.page(num)
    if num > 0 and num <= p.num_pages:
        return p.page(num)
    else:
        return p.page(1)

#Return the current page number (handle unxepected cases) and list of total number of pages
def pageNumber(objList,num):
    p = Paginator(objList,10)
    if num > 0:
        try:
            num = int(num)
        except:
            num = 1
    else:
        num = 1
    #print p.page(num)
    if num > 0 and num <= p.num_pages:
        return num,range(1,p.num_pages+1)
    else:
        return 1,range(1,p.num_pages+1)

@csrf_exempt
#Show all books (10 per page)
def booksList(request):
    dictV = {}
    books = Book.objects.all()
    coverPhotos = []
    num = request.GET.get('page') #Get page number from url
    order_list=['numPages', 'rating']
    res=Book.objects.order_by(*order_list)
    print res
    #Filter books based on selected filters
    gernres = request.GET.getlist('genres') #Selected filters/genres
    authors=request.GET.getlist('authors')
    ratings=request.GET.getlist('ratings')
   # print filters
    allFilters = Book.objects.order_by().values_list('genre',flat=True).distinct() #All possible filters/genres
    allauthors = Book.objects.order_by().values_list('author',flat=True).distinct() #All possible filters/authors
    allratings = Book.objects.order_by().values_list('rating',flat=True).distinct() #All possible filters/ratings
    selected = [ True for x in allFilters ]
    selectedauthor=[ True for x in allFilters ]
    selectedrating=[ True for x in allFilters ]
    if gernres:
        books = books.filter(genre__in = gernres)
        selected = [] #Whether a filter was selected or not
        for f in allFilters:
            if f in gernres: #If a genre/filter was selected
                selected.append(True)
            else:
                selected.append(False)
    if authors:
        books = books.filter(author__in = authors)
        selectedauthor = [] #Whether a filter was selected or not
        for f in allauthors:
            if f in gernres: #If a genre/filter was selected
                selectedauthor.append(True)
            else:
                selectedauthor.append(False)
    if ratings:
        books = books.filter(rating__in = ratings)
        selectedrating = [] #Whether a filter was selected or not
        for f in allauthors:
            if f in gernres: #If a genre/filter was selected
                selectedrating.append(True)
            else:
                selectedrating.append(False)


    dictV['allFilters'] = zip(allFilters,selected)
    dictV['allauthors'] = zip(allauthors,selectedauthor)
    dictV['allratings'] = zip(allratings,selectedrating)
    dictV['genres'] = gernres #Selected filters
    dictV['authors'] = authors #Selected filters
    dictV['ratings'] = ratings #Selected filters
    #Paginate: Show 10 books per page
    dictV['pageNumber'],dictV['totalPages'] = pageNumber(books,num) #Current Page number and list of total number of pages
    books = paginateObjects(books,num)
    for book in books:
        try:
            coverPhoto = CoverPhoto.objects.filter(book=book)[0]
        except:
            coverPhoto = None
        coverPhotos.append(coverPhoto)


    dictV['books'] = zip(books,coverPhotos)
    return render(request,'booksPage.html',dictV)

#Details of a specific book
def bookDetails(request,pk):
    dictV = {}
    try:
        userObj = BookedUser.objects.get(authUser=request.user)
    except:
        userObj = None
    book = Book.objects.get(pk=pk)
    book.rating = round(book.rating * 2) / 2 #Round it to 0.5
    if book.rating == int(book.rating):
        book.rating = int(book.rating)
    try:
        cover = CoverPhoto.objects.filter(book=book)[0].photo
        cover="/".join(str(img).split('/')[1:])
        print cover
    except:
        c=None
    dictV['conditions'] = conditionChoices #Conditions that a book can be in
    dictV['book'] = book
    dictV['coverPhoto'] = cover
    sellingPrice = ( book.cost * (100-book.discount) )/ 100
    dictV['sellingPrice'] = sellingPrice
    addedToCart = request.GET.get('addedToCart')
    q=CartBook.objects.filter(user=userObj, book=dictV['book'])
    if q:
       
         incart=1;    
    else:
        incart=request.GET.get('incart')
    p=RentItem.objects.filter(user=userObj, book=dictV['book'])
    if p:
       
         inrent=1;    
    else:
        inrent=request.GET.get('inrent')

    if request.user.is_authenticated():
        #check if the logged in user wants to lend a book or not and the book is not exchanged currently.
        b = possessedBooks.objects.filter(user=userObj,book=book,exchangedCurrently=False)
        if len(b):
            dictV['possessed'] = True
        else:
            dictV['possessed'] = False
    else:
        dictV['possessed'] = False

    #All lenders of a book
    people = [] #list of users who have not yet rented the book and possess it
    alreadyRequested = [] #Boolean list to check whether current book was already requested from a particular user or not
    q = possessedBooks.objects.filter(book=book,exchangedCurrently=False) #fetch possessedBooks objects for a particular book
    if userObj:
        q = q.exclude(user=userObj) #Show all users other than currently logged in user
    for x in q:
        people.append(x.user)
        exchangeObj = Exchange.objects.filter(book2=book,user1=userObj,user2=x.user)
        if len(exchangeObj):
            alreadyRequested.append(True)
        else:
            alreadyRequested.append(False)
    dictV['people'] = zip(people,alreadyRequested)

    dictV['addedToCart'] = addedToCart
    dictV['inCart']=incart
    dictV['inrent']=inrent
    dictV['form'] = opinionForm()
	
    reviews = []
    latest_review_list = opinion.objects.filter(book__pk = pk).order_by('-pub_date')
    for review in latest_review_list:
        likes = Reviewlike.objects.filter(opinions = review)
        reviews.append((review, likes))
	dictV['reviews']=reviews
           
    latest_discussion_list = []
    temp=Discuss.objects.filter(book_object_id=book.pk).order_by('-pub_date')
    for x in temp:
        if isinstance(x.book_object, Book):
            latest_discussion_list.append(x)
    discussions = []
    for discussion in latest_discussion_list:
        likes = Discusslike.objects.filter(discuss = discussion)
        fComments = []
        comments = DiscussionComment.objects.filter(discuss = discussion)
        for comment in comments:
            r = ReplyDcomment.objects.filter(reply = comment)
            fComments.append((comment, r))
        discussions.append((discussion, likes, fComments))
    #tags=Discuss.objects.get(pk=pk).discusstags.all()
    dictV['latest_discussion_list']=discussions
    #dictV['tags']=tags
    dictV['dform']=discussionForm
    if request.user.is_authenticated():  	
       if userObj.isSubscribed:
          dictV['isSubscribed']=1
    else:
       dictV['isSubscribed']=0
    return render(request,'book.html',dictV)
#SeriesPage
def seriesDetails(request,pk):
    dictV = {}
    try:
        userObj = BookedUser.objects.get(authUser=request.user)
    except:
        userObj = None
    series = Series.objects.get(pk=pk)
    series.seriesRating = round(series.seriesRating * 2) / 2 #Round it to 0.5
    if series.seriesRating == int(series.seriesRating):
        series.seriesRating = int(series.seriesRating)
    try:
        cover = SeriesPhoto.objects.filter(series=series).photo
        #cover="/".join(str(img).split('/')[1:])
        print cover
    except:
        cover=None
    bookslist=Book.objects.filter(series=pk)
    
    dictV['conditions'] = conditionChoices #Conditions that a book can be in
    dictV['series'] = series
    dictV['books'] = bookslist
    dictV['coverPhoto'] = cover
    sellingPrice = ( series.cost * (100-series.discount) )/ 100
    dictV['sellingPrice'] = sellingPrice
    addedToCart = request.GET.get('addedToCart')
    q=CartSeries.objects.filter(book=series)
    print q
    if q:
       
         incart=1;    
    else:
        incart=request.GET.get('incart')
    
    dictV['addedToCart'] = addedToCart
    dictV['inCart']=incart
    latest_discussion_list=Discuss.objects.filter(book_object_id=series.id).order_by('-pub_date')[:9]
    #tags=Discuss.objects.get(pk=pk).discusstags.all()
    dictV['latest_discussion_list']=latest_discussion_list
    #dictV['tags']=tags
    dictV['dform']=discussionForm
    	
    subscribed=Subscription.objects.filter(user=userObj)
    if subscribed:
         issubscribed=1
    else:
         issubscribed=0
    dictV['issubscribed']=issubscribed
    
    return render(request,'series.html',dictV)

@login_required
#Show the list of users who have a book and have put it up for exchanging
def bookExchange(request,pk):
    dictV = {}
    book = Book.objects.get(pk=pk)
    userObj = BookedUser.objects.get(authUser=request.user)
    people = [] #list of users who have not yet rented the book and possess it
    alreadyRequested = [] #Boolean list to check whether current book was already requested from a particular user or not
    q = possessedBooks.objects.filter(book=book,exchangedCurrently=False) #fetch possessedBooks objects for a particular book
    q = q.exclude(user=userObj) #Show all users other than currently logged in user
    for x in q:
        people.append(x.user)
        exchangeObj = Exchange.objects.filter(book2=book,user1=userObj,user2=x.user)
        if len(exchangeObj):
            alreadyRequested.append(True)
        else:
            alreadyRequested.append(False)

    dictV['book'] = book
    dictV['people'] = zip(people,alreadyRequested)
    return render(request,'bookExchange.html',dictV)

@login_required
#Show requests made by user, for the user and approved requests
def myExchanges(request):
    dictV = {}
    userObj = BookedUser.objects.get(authUser=request.user)
    #approved = Exchange.objects.filter(user2=userObj,approved=True,returnVerified=False) #exchanges that were approved by current user (show return button with books)
    pending = Exchange.objects.filter(user2=userObj,approved=False,disapproved=False) #pending requests for exchanges
    requestsByMe = Exchange.objects.filter(user1=userObj,approved=False) #requested by self

    #currently exchanged books
    currentExchanges = Exchange.objects.filter( Q(user1=userObj) | Q(user2=userObj) ) #Currently exchanged
    currentExchanges = currentExchanges.filter(approved=True,returnVerified=False)

    #previously exchanged books (that are returned and returnVerified)
    previousExchanges = Exchange.objects.filter( Q(user1=userObj) | Q(user2=userObj) ) #Currently exchanged
    previousExchanges = previousExchanges.filter(returnVerified=True)

    #dictV['approved'] = approved
    dictV['pending'] = pending
    dictV['requestsByMe'] = requestsByMe
    dictV['currentExchanges'] = currentExchanges
    dictV['previousExchanges'] = previousExchanges
    return render(request,'myExchanges.html',dictV)

@login_required
#Rent a book
def bookRent(request):
    dictV = {}
    userObj = BookedUser.objects.get(authUser=request.user)
    items = RentItem.objects.filter(user=userObj)
    totalCosts = []
    totalAmount = 0
    for i in items:
        totalCosts.append(i.book.rent+i.book.refundableSecurity)
        totalAmount += i.book.rent + i.book.refundableSecurity
    dictV['items'] = zip(items,totalCosts)
    dictV['totalAmount'] = totalAmount
    return render(request,'bookRent.html',dictV)

@login_required(login_url='/litwit/accounts/login/')
#Add a book to cart

def bookAddToWishlistBook(request,pk):
    if not request.user.is_authenticated():
        raise PermissionDenied
        return HttpResponseRedirect(login_url)
    dictV = {}
    book = Book.objects.get(pk=pk)
    userObj = BookedUser.objects.get(authUser=request.user)
    dictV['book'] = book
    q=wishlist.objects.filter(item=ContentType.objects.get_for_model(book),user=userObj,wishlist_object_id=book.pk)
    if q:
       dictV={}
       dictV['status']='Already In Wishlist'
       return JsonResponse(dictV)
    
       #return HttpResponseRedirect('/books/'+str(pk)+'?inwishlist=1')
    #Check if book is in stock and decrease numOfCopies by one
    item = wishlist.objects.create(item=ContentType.objects.get_for_model(book),user=userObj, wishlist_object_id=book.pk)
    dictV={}
    dictV['status']='Added To Wishlist'
    return JsonResponse(dictV)
    return HttpResponseRedirect('/')

@login_required
#Add a book to cart
def bookAddToCart(request,pk):
    dictV = {}
    book = Book.objects.get(pk=pk)
    userObj = BookedUser.objects.get(authUser=request.user)
    dictV['book'] = book
    q=CartBook.objects.filter(user=userObj,book=book)
    if q:
       return HttpResponseRedirect('/litwit/book/'+str(pk)+'?incart=1')
    #Check if book is in stock and decrease numOfCopies by one
    if book.inStock:
        item = CartBook.objects.create(user=userObj,book=book)
        book.numOfCopies -=1
        book.save()
        return HttpResponseRedirect('/litwit/book/'+str(pk)+'?addedToCart=1')
    else:
        return HttpResponseRedirect('/litwit/book/'+str(pk)+'?notInStock=1')

@login_required
#Remove book from cart
def bookRemoveFromCart(request,pk):
    dictV = {}
    item = CartBook.objects.get(pk=pk)
    item.delete()
    return HttpResponseRedirect('/litwit//book/cart')

@login_required
#Remove book from cart
def seriesRemoveFromCart(request,pk):
    dictV = {}
    item = CartSeries.objects.get(pk=pk)
    item.delete()
    return HttpResponseRedirect('/litwit/book/cart')

@login_required
#Add a book to rental cart
def bookAddToRentalCart(request,pk):
    dictV = {}
    book = Book.objects.get(pk=pk)
    userObj = BookedUser.objects.get(authUser=request.user)
    dictV['book'] = book
    #Check if book is in stock and decrease numOfCopies by one
    q=RentItem.objects.filter(user=userObj,book=book)
    if q:
       return HttpResponseRedirect('/litwit/book/'+str(pk)+'?inrent=1')
    
    if book.inStock:
        notify.send(request.user, recipient=request.user, verb='you added '+book.title)
        item = RentItem.objects.create(book=book,user=userObj)
        book.numOfCopies -=1
        book.save()
        book.save()
        return HttpResponseRedirect('/litwit/book/'+str(pk)+'?addedTorent=1')
    else:
        return HttpResponseRedirect('/litwit/book/'+str(pk)+'?notInStock=1')

@login_required
#Remove book from rental cart
def bookRemoveFromRentalCart(request,pk):
    dictV = {}
    item = RentItem.objects.get(pk=pk)
    item.delete()
    return HttpResponseRedirect('/litwit/book/cart')

@login_required
#Show cart
def bookCart(request):
    dictV = {}
    userObj = BookedUser.objects.get(authUser=request.user)
    rentItems = []
    buyItems=[]
    seriesItems=[]
    totalAmount = 0
    rentCost = 0
    buyCost = 0
    hardAmount = 0
    saveAmount=0
    items = CartBook.objects.filter(user=userObj)
    print items
    for i in items:
        try:
            img = CoverPhoto.objects.filter(book=i.book)[0].photo #Get the first cover photo of the image
            img1="/".join(str(img).split('/')[1:])

        except:
            img1 = ''
          
        totalAmount += (((100-i.book.discount)*i.book.cost)/100)*i.qty
        buyCost += (((100-i.book.discount)*i.book.cost)/100)*i.qty
        buyItems.append([i,img1,( (100-i.book.discount)*i.book.cost)/100] )
        hardAmount += i.book.cost
    sitems = CartSeries.objects.filter(user=userObj)
    print sitems
    for i in sitems:
        try:
            simg = SeriesPhoto.objects.filter(series=i.book.pk)[0].photo #Get the first cover photo of the image
            simg1="/".join(str(img).split('/')[1:])

        except:
            simg1 = ''
         
        print i.book  
        totalAmount += (((100-i.book.discount)*i.book.cost)/100)*i.qty
        buyCost += (((100-i.book.discount)*i.book.cost)/100)*i.qty
        seriesItems.append([i,simg1,( (100-i.book.discount)*i.book.cost)/100] )
        hardAmount += i.book.cost
    
    item = RentItem.objects.filter(user=userObj)
    numOfRentedBooks = len(item) #For calculating the base amount of renting
    rentCost=numOfRentedBooks*100
    print rentCost
    for i in item:
        try:
            img = CoverPhoto.objects.filter(book=i.book)[0].photo #Get the first cover photo of the image
            img1="/".join(str(img).split('/')[1:])

        except:
            img = ''
        
        #totalAmount += ((100-i.book.discount)*i.book.cost)/100
        rentItems.append([i,img1,( (100-i.book.discount)*i.book.cost)/100] )

    dictV['saveAmount'] = hardAmount-totalAmount
    totalAmount+=rentCost
    dictV['items'] = buyItems
    dictV['series'] = seriesItems
    dictV['rentitems'] = rentItems
    dictV['totalAmount'] = totalAmount
    dictV['numOfRentedBooks'] = numOfRentedBooks
    dictV['rentCost']  = rentCost
    dictV['buyCost'] = buyCost

    return render(request,'cart.html',dictV)
@login_required(login_url='/litwit/accounts/login/')
#Add a book to cart

def bookAddTosubscriptionlist(request,pk):
    if not request.user.is_authenticated():
        raise PermissionDenied
        return HttpResponseRedirect(login_url)
    dictV = {}
    book = Book.objects.get(pk=pk)
    userObj = BookedUser.objects.get(authUser=request.user)
    q=Subscription.objects.filter(user=userObj)
    if not q:
       return HttpResponseRedirect('/litwit/subscribe/')
    dictV['book'] = book
    item = Subscriptionlist.objects.filter(book=book,user=userObj)
    if item:
       dictV={}
       dictV['status']='Already In Subscription Cart'
       return JsonResponse(dictV)
    
       #return HttpResponseRedirect('/books/'+str(pk)+'?inwishlist=1')
    #Check if book is in stock and decrease numOfCopies by one
    item = Subscriptionlist.objects.create(book=book,user=userObj)
    dictV={}
    dictV['status']='Added To subscription Cart'
    return JsonResponse(dictV)
    return HttpResponseRedirect('/')


@login_required
def subscriptiondisplay(request):
    dictV = {}
    userObj = BookedUser.objects.get(authUser=request.user)
    buyItems=[]
    items = Subscriptionlist.objects.filter(user=userObj)
    for i in items:
        try:
            img = CoverPhoto.objects.filter(book=i.book)[0].photo #Get the first cover photo of the image
            img1="/".join(str(img).split('/')[1:])

        except:
            img1 = ''
        print img
        buyItems.append([i,img1] )
    
    dictV['items']=buyItems
    return render(request,'subscription_cart.html',dictV)

@login_required
def subscriptiondeliver(request,pk):
    dictV = {}
    buyItems=[]
    userObj = BookedUser.objects.get(authUser=request.user)
    items = Subscriptionlist.objects.filter(user=userObj,delievered=True)
    noOfItems=len(items)
    print noOfItems
    if noOfItems < 4:
       items = Subscriptionlist.objects.get(pk=pk)

       items.delievered=True
       items.save()
    items = Subscriptionlist.objects.filter(user=userObj)
    for i in items:
        try:
            img = CoverPhoto.objects.filter(book=i.book)[0].photo #Get the first cover photo of the image
            img1="/".join(str(img).split('/')[1:])

        except:
            img1 = ''
        print img
        buyItems.append([i,img1] )
    
    dictV['items']=buyItems
    

    return render(request,'subscription_cart.html',dictV)

def subscriptionplan(request):
    dictV = {}
    if request.user.is_authenticated():
       if request.GET.get('book'):
          bookId=request.GET.get('book')
          userObj = BookedUser.objects.get(authUser=request.user) 
          book = Book.objects.get(pk=bookId)
          q=Subscriptionlist.objects.filter(user=userObj,book=book)
          if q:
             return HttpResponseRedirect('/litwit/subscribe')
          Subscriptionlist.objects.create(user=userObj,book=book)     
          print bookId
       if request.GET.get('series'):
          seriesId=request.GET.get('series')
          print seriesId

    return render(request,'subscriptionplan.html',dictV)
@login_required
def subscriptionconfirmation(request,apk):
    dictV = {}
    userObj = BookedUser.objects.get(authUser=request.user)
    totalAmount=0
    rentAmount=0;
    
    plan=SubscriptionPlan.objects.get(pk=apk)
    dictV['plan']=plan  
    dictV['amount']=plan.amount  
    return render(request,'paymentsubscription.html',dictV)    
    


@login_required
def bookwishlist(request):
    dictV = {}
    userObj = BookedUser.objects.get(authUser=request.user)
    
    items = wishlist.objects.filter(user=userObj)
    dictV['items']=items
    return render(request,'wishlist.html',dictV)

@login_required
def shipmentaddress(request):
    dictV={}
    userObj = BookedUser.objects.get(authUser=request.user)
    addresses=UserAddress.objects.filter(user=userObj)
    dictV['addresses']=addresses
    if request.GET.get('addressPk'):
       addressPk = request.GET.get('addressPk')
       cont=1
       dictV['addressPk']=addresses
       dictV['cont']=addresses
    else:
       cont= None
       dictV['cont']=addresses
    return render(request,'address.html',dictV)
@login_required
def subscriptionaddress(request):
    dictV={}
    userObj = BookedUser.objects.get(authUser=request.user)
    addresses=UserAddress.objects.filter(user=userObj)
    dictV['addresses']=addresses
    if request.GET.get('addressPk'):
       addressPk = request.GET.get('addressPk')
       cont=1
       dictV['addressPk']=addresses
       dictV['cont']=addresses
    else:
       cont= None
       dictV['cont']=addresses
    return render(request,'addressSubscription.html',dictV)

@login_required
def reviewSubscription(request,apk):
    booksList=list()
    userObj = BookedUser.objects.get(authUser=request.user)
    books=Subscriptionlist.objects.filter(user=userObj,delievered=True)
    for b in books:
            try:
                coverPhoto = CoverPhoto.objects.filter(book=b.book)[0].photo
            except CoverPhoto.DoesNotExist:
                coverPhoto = None
            booksList.append((b.book,coverPhoto))
    context = {
        "books" : booksList,
        "addId" : apk,
        }
    return render(request,'review_subscription.html',context)    


@login_required
def reviewOrder(request,apk):
    booksList=list()
    userObj = BookedUser.objects.get(authUser=request.user)
    books=CartBook.objects.filter(user=userObj)
    buyCost=0
    actCost=0
    for b in books:
            buyCost = (((100-b.book.discount)*b.book.cost)/100)*b.qty 
            actCost=(b.book.cost)*b.qty
            try:
                coverPhoto = CoverPhoto.objects.filter(book=b.book)[0].photo
            except CoverPhoto.DoesNotExist:
                coverPhoto = None
            booksList.append((b.book,buyCost,actCost,coverPhoto))
    series=CartSeries.objects.filter(user=userObj)
    seriesList=list()
    for s in series:
            buyCost = (((100-s.book.discount)*s.book.cost)/100)*s.qty 
            actCost=s.book.cost*s.qty
            
            try:
                coverPhoto = SeriesPhoto.objects.filter(series=s.book)[0].photo
            except CoverPhoto.DoesNotExist:
                coverPhoto = None
            seriesList.append((s,buyCost,actCost,coverPhoto))
    rentList=list()
    rent=RentItem.objects.filter(user=userObj)
    for r in rent:
            try:
                coverPhoto = CoverPhoto.objects.filter(book=r.book)[0].photo
            except CoverPhoto.DoesNotExist:
                coverPhoto = None
            rentList.append((r,coverPhoto))
    context = {
        "books" : booksList,
        "series" : seriesList,
        "rent" : rentList,
        "addId" : apk,
        }
    return render(request,'review_order.html',context)    

@login_required
def orderconfirmation(request,apk):
    dictV = {}
    userObj = BookedUser.objects.get(authUser=request.user)
    books=CartBook.objects.filter(user=userObj)
    totalAmount=0
    rentAmount=0;
    for item in books:
            totalAmount += (100-item.book.discount)*item.book.cost/100
    series=CartSeries.objects.filter(user=userObj)
    for s in series:
            totalAmount += (100-s.book.discount)*s.book.cost/100
    rent=RentItem.objects.filter(user=userObj)
    rentAmount=len(rent)*100
    totalAmount+=rentAmount
    address=UserAddress.objects.get(pk=apk)
    dictV['totalAmout']=totalAmount  
    dictV['address']=address  
    return render(request,'payment.html',dictV)   
 
@login_required    
def subscriptionorderconfirmation(request,apk):
    dictV = {}
    userObj = BookedUser.objects.get(authUser=request.user)

    totalAmount=0
    rentAmount=0
    address=UserAddress.objects.get(pk=apk)
    subscription=Subscription.objects.get(user=userObj)
    shipmentObj=subscription.shipment
    shipmentObj.address=address
    shipmentObj.save()
    #subscription.save()
    sublist=Subscriptionlist.objects.filter(user=userObj, delievered=True)
    print shipmentObj
    for s in sublist:
        SubscriptionPurchase.objects.create(user=userObj, book=s.book,shipment=shipmentObj,activity_time=datetime.datetime.now())
       
    return HttpResponseRedirect('/litwit/book/cart')

    

@login_required
#Send an otp via sms and store it in database
def sendOtp(request):
    dictV = {}
    userObj = BookedUser.objects.get(authUser=request.user)
    phone = userObj.phone
    otp = str( (uuid.uuid1().int)%(10**6) ) #generate a six digit unique otp

    #Send otp as sms and email
    print otp
    content = 'Your One Time Password for Litwit is '+otp+'. \nLitwit'
    subject = 'One Time Password | Litwit'

    #Email Otp
    sendEmail( request.user.email, subject, content)

    #SMS Otp
    try:
        sendSms([ userObj.phone, ],content)
        dictV['status'] = 'sent'
    except:
        dictV['status'] = 'not sent'

    #Update otp in user model
    userObj.otp = otp
    userObj.canBorrow = False #Set canBorrow to False whenever new otp is generated: set to True only when otp is verified; after using it set to False again
    userObj.save()

    return JsonResponse(dictV)
def sample(request):
    dictV={}
    dictV['data']='hey'
    return JsonResponse(dictV)
#Verify one time password,create shipment and purchases objects
@login_required
@csrf_exempt
def verifyOtp(request):
    dictV = {}
    userObj = BookedUser.objects.get(authUser=request.user)
    userBookCart = CartBook.objects.filter(user=userObj)
    userSeriesCart = CartSeries.objects.filter(user=userObj)
    userRent = RentItem.objects.filter(user=userObj)
    paymentOption = request.POST.get('paymentOption')
    shippingAddressId = request.POST.get('address')
    shippingAddress = UserAddress.objects.get(pk=shippingAddressId)
    #startDate = request.POST.get('startDate')
    startDate = timezone.now() + timezone.timedelta(days=14) #Set startDate to two weeks from now
    #If address is left blank, set the shippingAddress to user's address
    if shippingAddress == '' or shippingAddress is None:
        shippingAddress = userObj.address
    sentOtp = request.POST.get('otp')
    savedOtp = userObj.otp
    if str(savedOtp) == str(sentOtp):
        userObj.otp = '' #Reset the stored otp once it is verified
        if paymentOption == 'COD':
            userObj.canBorrow = False #Set to false after checkout in razorpay
        userObj.save()
        #Calculate total cost
        totalCost = 0
        for item in userBookCart:
            totalCost += ((100-item.book.discount)*item.book.cost/100)*item.qty

        for item in userSeriesCart:
            totalCost += ((100-item.book.discount)*item.book.cost/100)*item.qty

        
        totalCost += len(userRent)*100 #Rent Cost

        #Calculate rent and amount to be refunded to the user
        #Basic Rent = 60 for one book, 110 for two books, 150 for three books, 190 for four books
        #Total Rent = Basic Rent + duration or (Basic Rent + duration*2) for 4 or more books
        numOfRentedBooks = len(userRent) #Number of rented books taken by user for renting
        refundableAmount = 0
        rent=0
        for item in userRent:
            refundableAmount += (100-item.book.discount)*item.book.cost/100
        #Refundable Amount = Total Amount of Rented Books - Rent
        refundableAmount -= rent

        #Create a shipment object for the whole order
        shipmentObj = Shipment.objects.create(shippingAddress = shippingAddress, paymentOption=paymentOption,user=userObj,totalCost=totalCost,refundableAmount = refundableAmount)

        #Create Rental and Purchases model objects

        #Create a Purchases object for each book in cart
        for item in userBookCart:
            purchaseObj = Purchases.objects.create(book=item.book,cost=(100-item.book.discount)*item.book.cost/100,shipment=shipmentObj)
        #If method is COD, empty the cart and set transactionComplete to True
        if paymentOption == 'COD':
            shipmentObj.transactionComplete = True
            shipmentObj.razorpayId = 'COD'+str(shipmentObj.pk)
            shipmentObj.save()
            #Delete books from cart
            for item in userBookCart:
                item.delete()
        for item in userSeriesCart:
            purchaseObj = SeriesPurchases.objects.create(series=item.book,cost=(100-item.book.discount)*item.book.cost/100,shipment=shipmentObj)
        #If method is COD, empty the cart and set transactionComplete to True
        if paymentOption == 'COD':
            shipmentObj.transactionComplete = True
            shipmentObj.razorpayId = 'COD'+str(shipmentObj.pk)
            shipmentObj.save()
            #Delete books from cart
            for item in userSeriesCart:
                item.delete()

        #Create a Rental object for each book in RentItem
        for item in userRent:
            rentObj = Rental.objects.create(book=item.book,shipment=shipmentObj,duration=30,startDate=startDate)

        #If method is COD, empty the cart and set transactionComplete to True
        if paymentOption == 'COD':
            shipmentObj.transactionComplete = True
            shipmentObj.razorpayId = 'COD'+str(shipmentObj.pk)
            shipmentObj.save()
            #Delete books from rental cart
            for item in userRent:
                item.delete()

        dictV['status'] = 'Verified'
        dictV['id'] = shipmentObj.pk #Send back the pk of shipment object just created
        dictV['amount'] = totalCost #Send back the pk of shipment object just created

    else:
        dictV['status'] = 'Not Verified'
    return JsonResponse(dictV)
@login_required
@csrf_exempt
def codsubscription(request):
    dictV = {}
    userObj = BookedUser.objects.get(authUser=request.user)
    paymentOption = request.POST.get('paymentOption')
    plan = request.POST.get('plan')
    plan=SubscriptionPlan.objects.get(pk=plan)
    shipmentObj = Shipment.objects.create(shippingAddress = None, paymentOption=paymentOption,user=userObj,totalCost=plan.amount)
    Subscription.objects.create(user=userObj,plan=plan,shipment=shipmentObj)
    if paymentOption == 'COD':

           shipmentObj.transactionComplete = True
           shipmentObj.razorpayId = 'COD'+str(shipmentObj.pk)
           shipmentObj.save()
            
           userObj.isSubscribed = True
           userObj.save()

           dictV['status'] = 'Verified'
           dictV['id'] = shipmentObj.pk #Send back the pk of shipment object just created
           dictV['amount'] = plan.amount #Send back the pk of shipment object just created

    else:
        dictV['status'] = 'Verified'
    return JsonResponse(dictV)

#Callback after successful payment via razorpay
@login_required
@csrf_exempt
def onlinePayment(request):
    paymentId = request.POST.get('paymentId',None)
    shipmentId = request.POST.get('shipmentId')
    shipmentObj = Shipment.objects.get(pk=shipmentId)
    userObj = BookedUser.objects.get(authUser=request.user)
    userCart = CartItem.objects.filter(user=userObj)
    rentalCart = RentItem.objects.filter(user=userObj)

    #Set userObj.canBorrow to False
    userObj.otp = ''
    userObj.canBorrow = False



    userObj.save()

    #Set shipmentObj.paymentMade to True
    shipmentObj.paymentMade = True
    #Set shipmentObj.razorpayId, depending on COD or razorpay
    print paymentId
    if paymentId is not None:
        shipmentObj.razorpayId = paymentId
        #Set total cost to actual amount paid by user
        try:
            paymentObj = client.payment.fetch("paymentId")
            shipmentObj.totalCost = paymentObj['amount']
        except Exception as e:
            print str(e)
    else:
        shipmentObj.razorpayId = 'COD'+str(shipment.pk)

    #Delete books from cart
    for item in userCart:
        item.delete()

    #Delete books from rental cart
    for item in rentalCart:
        item.delete()

    shipmentObj.transactionComplete = True
    shipmentObj.save()
    #Send shipment.pk
    return HttpResponseRedirect('/books/orderComplete/?shipmentPk='+str(shipmentObj.pk) )

@login_required
@csrf_exempt
def verifyOtpQuick(request):
    dictV = {}
    userObj = BookedUser.objects.get(authUser=request.user)
    bookId = int(request.POST.get('bookId'))
    buyOrRent = request.POST.get('buyOrRent')
    bookObj = get_object_or_404(Book,pk=bookId)
    paymentOption = request.POST.get('paymentOption')
    shippingAddress = request.POST.get('address')
    duration = request.POST.get('duration')
    #startDate = request.POST.get('startDate')
    startDate = timezone.now() + timezone.timedelta(days=14) #Set startDate to two weeks from now
    #If address is left blank, set the shippingAddress to user's address
    if shippingAddress == '' or shippingAddress is None:
        shippingAddress = userObj.address
    sentOtp = request.POST.get('otp')
    savedOtp = userObj.otp
    if str(savedOtp) == str(sentOtp):
        userObj.otp = '' #Reset the stored otp once it is verified
        if paymentOption == 'COD':
            userObj.canBorrow = False #Set to false after checkout in razorpay
        userObj.save()

        #Create a shipment object for the whole order
        #Calculate total cost
        totalCost = (100-bookObj.discount)*bookObj.cost/100

        #Calculate refundable amount
        if buyOrRent == 'Rent':
            #For one book, rentAmount = 60 + duration
            rentAmount = 60 + int(duration)
            if rentAmount > (100-bookObj.discount)*bookObj.cost/100:
                rentAmount = (100-bookObj.discount)*bookObj.cost/100
            refundableAmount = (100-bookObj.discount)*bookObj.cost/100 - rentAmount
        else:
            refundableAmount = 0

        shipmentObj = Shipment.objects.create(shippingAddress = shippingAddress, paymentOption=paymentOption,user=userObj,totalCost=totalCost,refundableAmount = refundableAmount)

        #create Rental or Purchases model object
        if buyOrRent == 'Buy':
            purchaseObj = Purchases.objects.create(book=bookObj,cost=totalCost,shipment=shipmentObj)
        else:
            rentObj = Rental.objects.create(book=bookObj,shipment=shipmentObj,duration=duration,startDate=startDate)

        #If method is COD, empty the cart and set transactionComplete to True
        if paymentOption == 'COD':
            shipmentObj.transactionComplete = True
            shipmentObj.razorpayId = 'COD'+str(shipmentObj.pk)
            shipmentObj.save()

        dictV['status'] = 'Verified'
        dictV['id'] = shipmentObj.pk #Send back the pk of shipment object just created
        dictV['amount'] = totalCost #Send back the pk of shipment object just created

    else:
        dictV['status'] = 'Not Verified'
    return JsonResponse(dictV)

@login_required
@csrf_exempt
def onlinePaymentQuick(request):
    paymentId = request.POST.get('paymentId',None)
    shipmentId = request.POST.get('shipmentId')
    shipmentObj = Shipment.objects.get(pk=shipmentId)
    userObj = BookedUser.objects.get(authUser=request.user)
    bookId = request.POST.get('bookId')
    buyOrRent = request.POST.get('buyOrRent')
    bookObj = get_object_or_404(Book,pk=bookId)

    #Set userObj.canBorrow to False
    userObj.otp = ''
    userObj.canBorrow = False
    userObj.save()

    #Set shipmentObj.paymentMade to True
    shipmentObj.paymentMade = True
    #Set shipmentObj.razorpayId, depending on COD or razorpay
    if paymentId is not None:
        shipmentObj.razorpayId = paymentId
        #Set total cost to actual amount paid by user
        try:
            paymentObj = client.payment.fetch("paymentId")
            shipmentObj.totalCost = paymentObj['amount']
        except Exception as e:
            print str(e)
    else:
        shipmentObj.razorpayId = 'COD'+str(shipment.pk)

    shipmentObj.transactionComplete = True
    shipmentObj.save()
    #Send shipment.pk
    return HttpResponseRedirect('/books/orderComplete/?shipmentPk='+str(shipmentObj.pk) )

#Success page: called directly after COD or after onlinePayment was called
@login_required
@csrf_exempt
def orderComplete(request):
    dictV = {}
    shipmentPk = request.GET.get('shipmentPk')
    if not shipmentPk:
        return HttpResponseRedirect('/books/orderUnsuccessful/')
    try:
        shipmentObj = Shipment.objects.get(pk=shipmentPk)
        dictV['id'] = Shipment.pk
        dictV['paymentId'] = shipmentObj.razorpayId
        rentBooks = []
        buyBooks = []
        rentObj = Rental.objects.filter(shipment = shipmentObj)
        buyObj = Purchases.objects.filter(shipment = shipmentObj)
        for r in rentObj:
            rentBooks.append(r.book)
        for b in buyBooks:
            buyBooks.append(b.book)

        bPhotos = []
        rPhotos = []

        for book in rentBooks:
            try:
                coverPhoto = CoverPhoto.objects.filter(book=book)[0]
            except CoverPhoto.DoesNotExist:
                coverPhoto = None
            bPhotos.append(coverPhoto)

        for book in buyBooks:
            try:
                coverPhoto = CoverPhoto.objects.filter(book=book)[0]
            except CoverPhoto.DoesNotExist:
                coverPhoto = None
            rPhotos.append(coverPhoto)

        #print rentBooks, buyBooks
        dictV['rentBooks'] = zip(rentBooks,rPhotos)
        dictV['buyBooks'] = zip(buyBooks,bPhotos)


        #Send email of confirmation
        content = '<p>Your Order has been confirmed.</p><p>Reference number: '+ shipmentObj.razorpayId + '</p><p><i>Team Litwit</i></p>'
        subject = 'Shipment Details | Litwit'
        sendEmail( request.user.email, subject, content)

        return render(request,'orderComplete.html',dictV)
    except Shipment.DoesNotExist:
        return HttpResponseRedirect('/books/orderUnsuccessful/')

#In case of unsuccessful payment, redirect user here
@login_required
def orderUnsucessful(request):
    dictV = {}
    return render(request,'orderUnsucessful.html',dictV)

def login(request):
    dictV = {}
    dictV.update(csrf(request))
    nextUrl = request.GET.get('next','/')
    dictV['next'] = nextUrl

    if request.user.is_authenticated():
        return HttpResponseRedirect(nextUrl)
    if request.method == 'POST':
        nextUrl = request.POST.get('next','/')
        dictV['loginForm'] = loginForm(request.POST)
        if dictV['loginForm'].is_authenticated():
            user = auth.authenticate(username=dictV['loginForm'].cleaned_data['email'], password=dictV['loginForm'].cleaned_data['password'])
            if user is not None:
                auth.login(request,user)
                return HttpResponseRedirect(nextUrl)
            else:
                dictV['error'] = 'Incorrect Username/Password'
        else:
            dictV['error'] = 'Incorrect Username/Password'
    else:
        dictV['loginForm'] = loginForm()
    return render(request,'login.html',dictV)

def logout(request):
    auth.logout(request)
    nextUrl = request.GET.get('next','/')
    return HttpResponseRedirect(nextUrl)

#View profile of the current user
@login_required
def viewProfile(request):
    dictV = {}
    #If BookedUser does not exist (social login), create it.
    try:
        dictV['bookedUser'] = BookedUser.objects.get(authUser=request.user)
    except BookedUser.DoesNotExist:
        #Try to get details from social account.
        try:
            socialAccounts = SocialAccount.objects.filter(user=request.user).order_by('-id')[0] #Get the latest social account
            name = socialAccounts.extra_data.get('name','')
        except Exception as e:
            print str(e)
            name = ''
        bookedUserObj = BookedUser.objects.create(authUser=request.user,name=name,penName=name)
        dictV['bookedUser'] = bookedUserObj
    return render(request,'viewProfile.html',dictV)

#Edit profile of current user
@login_required
def editProfile(request):
    dictV = {}
    dictV.update(csrf(request))
    bookedUser = BookedUser.objects.get(authUser=request.user)

    if request.method == 'POST':
        form = bookedUserForm(request.POST,request.FILES,instance=bookedUser)
        if form.is_valid():
            obj = form.save(commit=True)
            dictV['Success'] = True
            return HttpResponseRedirect('/accounts/profile')
        else:
            dictV['Failure'] = True
            dictV['error'] = form.errors

    dictV['bookedUserForm'] = bookedUserForm(instance=bookedUser)
    return render(request,'editProfile.html',dictV)


def register(request):
    dictV = {}
    dictV.update(csrf(request))
    nextUrl = '/accounts/editProfile'
    if request.method == 'POST':
        email = request.POST.get('email')
        username = request.POST.get('email')
        password = request.POST.get('password')
        password1 = request.POST.get('password1')
        regForm = RegistrationForm(request.POST)
        form = bookedUserForm(request.POST)
        if regForm.is_valid():
            if password == password1:
                authUser = auth.models.User.objects.create_user(username = email, password = password,email=email)
                bookedUserObj = BookedUser.objects.create(authUser=authUser)
                dictV['Success'] = True #NOTE: COMMENT IT IF USING BELOW CODE
                # if form.is_valid():
                #     obj = form.save(commit=False)
                #     obj.authUser = authObj
                #     obj.save()
                #     dictV['Success'] = True
                # else:
                #     dictV['Failure'] = True
                #     dictV['error'] = form.errors

                #Automatically log in a user on registration
                user = auth.authenticate(username = regForm.cleaned_data['username'], password=regForm.cleaned_data['password'])
                auth.login(request,user)
                return HttpResponseRedirect(nextUrl)
            else:
                dictV['Failure'] = True
                dictV['error'] = "Passwords Do Not Match. Please Try Again."
        else:
            print regForm.errors
            dictV['Failure'] = True
            dictV['error'] = regForm.errors

    dictV['authUserForm'] = RegistrationForm()
    dictV['bookedUserForm'] = bookedUserForm()
    return render(request,'register.html',dictV)

#Search results
def search(request):
    dictV = {}
    query = request.GET.get('q','')
    #searchType = request.GET.get('type','') #Search book/People
    searchType='people'
    num = request.GET.get('page') #Get page number from url

    if searchType == 'people':
        people = BookedUser.objects.filter( Q(name__icontains=query) |  Q(penName__icontains=query))
        if request.user.is_authenticated():
           fol=Follow.objects.filter(following=get_object_or_404( BookedUser,authUser=request.user ))
           dictV['fol']=fol
           dictV['upk']=BookedUser.objects.get(authUser=request.user).id
        #Paginate: Show 10 people per page
        people = paginateObjects(people,num)


        dictV['people'] = people
      
        dictV['pageNumber'],dictV['totalPages'] = pageNumber(people,num) #Current Page number and list of total number of pages

    else:
        books = Book.objects.filter( Q(title__icontains=query) | Q(author__icontains=query) | Q(isbn__icontains=query) )
        coverPhotos = []
        #Paginate: Show 10 books per page
        books = paginateObjects(books,num)
        for book in books:
            try:
                coverPhoto = CoverPhoto.objects.filter(book=book)[0]
            except CoverPhoto.DoesNotExist:
                coverPhoto = None
            coverPhotos.append(coverPhoto)
        dictV['books'] = zip(books,coverPhotos)
        dictV['pageNumber'],dictV['totalPages'] = pageNumber(books,num) #Current Page number and list of total number of pages

    dictV['type'] = searchType
    dictV['query'] = query
    return render(request,'search.html',dictV)

    # dictV = serializers.serialize('json',books)
    # return HttpResponse(dictV,content_type='json')

#Put up a book for lending: create a possessedBooks object or update an existing one
@login_required
def lendIt(request, pk):
    book = get_object_or_404(Book, pk=int(pk))
    nextUrl = request.GET.get('next','/litwit/book/'+str(pk) )
    revoke = request.GET.get('revoke','')
    condition = request.GET.get('condition','')
    userObj = BookedUser.objects.get(authUser=request.user)
    #Do not put it for lending: delete the possessedBooks object
    if revoke == '1':
        try:
            possessedBooksObjects = possessedBooks.objects.filter(user=userObj,book=book,exchangedCurrently=False).delete()
        except possessedBooks.DoesNotExist:
            pass
    #Put it for lending: create a possessedBooks object if it does not exist already
    else:
        possessedBooksObjects = possessedBooks.objects.filter(user=userObj,book=book,exchangedCurrently=False)
        #if such an object exists, do not create another one
        if len(possessedBooksObjects):
            pass
        else:
            possessedBooks.objects.create(user=userObj,book=book,condition=condition)
    return HttpResponseRedirect(nextUrl)

#Currently logged in user wants to get book having bookId from user having userId. Show own books, select one to exchange (optional).
@login_required
def bookExchangeConfirm(request,bookId,userId):
    dictV = {}
    dictV.update(csrf(request))
    userObj = get_object_or_404( BookedUser,pk=int(userId) )
    currentUser = get_object_or_404( BookedUser,authUser=request.user )
    bookObj = get_object_or_404( Book,pk=int(bookId) )
    dictV['form'] = exchangeForm()
    #print bookObj
    if request.method == 'POST':
        form = exchangeForm(request.POST,request.FILES)
        #If form is valid, save the exchange object
        if form.is_valid():
            obj = form.save(commit=False)
            obj.user1 = currentUser #logged in user
            #obj.book1: from form (optional)
            obj.user2 = userObj #from userId
            obj.book2 = bookObj #from bookId
            #obj.message: from form
            obj.save()
            #Create a message and send to user2
            messageObj = Message.objects.create(subject = 'Book Request: '+ str(obj.book2.title) ,body = obj.message,sender = obj.user1.authUser,recipient = obj.user2.authUser,sent_at = timezone.now() )
            dictV['Success'] = True
        else:
            print form.errors
            dictV['Failure'] = True
            dictV['error'] = form.errors

    dictV['book'] = bookObj
    dictV['person'] = userObj
    return render(request,'bookExchangeConfirm.html',dictV)

#Approve an exchange (by user2)
@login_required
def bookExchangeApprove(request,exchangeId):
    exchangeObj = get_object_or_404(Exchange,pk=int(exchangeId))
    userObj = BookedUser.objects.get(authUser=request.user)
    if exchangeObj.user2 == userObj:
        exchangeObj.approved = True
        exchangeObj.save()
        return HttpResponseRedirect('/accounts/myExchanges')
        #return JsonResponse({ "status":"success" })
    else:
        return JsonResponse({ "status":"failure","message":"You are not authorized to perform this action" },status=401)

#Disapprove an exchange (by user2)
@login_required
def bookExchangeDisapprove(request,exchangeId):
    exchangeObj = get_object_or_404(Exchange,pk=int(exchangeId))
    userObj = BookedUser.objects.get(authUser=request.user)
    if exchangeObj.user2 == userObj:
        exchangeObj.disapproved = True
        exchangeObj.save()
        return HttpResponseRedirect('/accounts/myExchanges')
        #return JsonResponse({ "status":"success" })
    else:
        return JsonResponse({ "status":"failure","message":"You are not authorized to perform this action" },status=401)

#Return an exchanged book (by user2)
@login_required
def bookExchangeReturn(request,exchangeId):
    exchangeObj = get_object_or_404(Exchange,pk=int(exchangeId))
    userObj = BookedUser.objects.get(authUser=request.user)
    if exchangeObj.user2 == userObj:
        exchangeObj.returned = True
        exchangeObj.save()
        return HttpResponseRedirect('/accounts/myExchanges')
        #return JsonResponse({ "status":"success" })
    else:
        return JsonResponse({ "status":"failure","message":"You are not authorized to perform this action" },status=401)

#Verify that the book has beeen returned (by user1)
@login_required
def bookExchangeReturnVerify(request,exchangeId):
    exchangeObj = get_object_or_404(Exchange,pk=int(exchangeId))
    userObj = BookedUser.objects.get(authUser=request.user)
    if exchangeObj.user1 == userObj:
        exchangeObj.returnVerified = True
        exchangeObj.save()
        return HttpResponseRedirect('/accounts/myExchanges')
        #return JsonResponse({ "status":"success" })
    else:
        return JsonResponse({ "status":"failure","message":"You are not authorized to perform this action" },status=401)

#View profile of other users
@login_required
def visitProfile(request, pk):
    dictV = {}
    dictV.update(csrf(request))
    bookedUser = get_object_or_404(BookedUser,pk=pk)
    dictV['person'] = bookedUser
    return render(request,'visitProfile.html',dictV)


    return render(request, 'bookDetails.html', {'Book': book, 'form': form})

#Add to review
#def addReview(request,pk):
    
 #   book = Book.objects.get(pk=pk)
  #  form = reviewForm(request.POST)
  #  dictV = {}
  #  dictV['book']=book

   # if form.is_valid():
   #     rating = form.cleaned_data['rating']
   #    comment = form.cleaned_data['comment']
   #     form.book = book
   #     form.rating = rating
   #     form.comment = comment
   #     form.pub_date = datetime.datetime.now()

        #form.save()

        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
   # return HttpResponseRedirect('/books/'+str(pk)+'?addedReview=1')


def bookAddToReadList(request,pk):
    if not request.user.is_authenticated():
        raise PermissionDenied
        return HttpResponseRedirect(login_url)
    dictV = {}
    book = Book.objects.get(pk=pk)
    userObj = BookedUser.objects.get(authUser=request.user)
    dictV['book'] = book
    q=readBooks.objects.filter(user=userObj,book=book)
    if q:
       dictV={}
       dictV['status']='Already In Read List'
       return JsonResponse(dictV)
    
    item = readBooks.objects.create(user=userObj,book=book)
    dictV={}
    dictV['status']='Added To Read List'
    return JsonResponse(dictV)
    return HttpResponseRedirect('/')


def bookAddToWishlist(request,pk):
    dictV = {}
    book = Book.objects.get(pk=pk)
    userObj = BookedUser.objects.get(authUser=request.user)
    dictV['book'] = book
    item = wishlist.objects.create(book=book,user=userObj)
    
    return HttpResponseRedirect('/books/'+str(pk)+'?addedToWishlist=1')
    

def searchwishlist(request):
    dictV = {}
    query = request.GET.get('q','')
    searchType = 'book' #Search book/People
    num = request.GET.get('page') #Get page number from url

    books = Book.objects.filter( Q(title__icontains=query) | Q(author__icontains=query) | Q(isbn__icontains=query) )
    coverPhotos = []
        #Paginate: Show 10 books per page
    books = paginateObjects(books,num)
    dictV['books'] = books
    dictV['pageNumber'],dictV['totalPages'] = pageNumber(books,num) #Current Page number and list of total number of pages

    dictV['type'] = searchType
    dictV['query'] = query
    return render(request,'searchwishlist.html',dictV)
@login_required
def likeReview(request,pk,opk):    
    book = get_object_or_404(Book, pk=pk)
    review = get_object_or_404(opinion, pk=opk)
    print review.user_name
    user = auth.models.User.objects.get(username=review.user_name.authUser)
    print review

    reviewd=opinion.objects.filter(pk=opk).update(likecount=F("likecount") + 1)
    print review.likecount
    userObj = BookedUser.objects.get(authUser=request.user)
    item=Reviewlike.objects.create(user=userObj, opinions=review)
    Activity.objects.create(actor=userObj, verb='Liked a review titled',action_object=review)    
         
    notify.send(userObj, recipient=user, target=review.user_name.authUser, verb='liked your review for the book'+book.title, action_object=review)  
    dictV={}
    dictV['data']=review.likecount +1
    return JsonResponse(dictV)
    return HttpResponseRedirect('/books/'+str(pk)+'/')
@login_required
def unlikeReview(request,pk,opk):    
    book = get_object_or_404(Book, pk=pk)
    review = get_object_or_404(opinion, pk=opk)
    print review.user_name
    user = auth.models.User.objects.get(username=review.user_name.authUser)
    print review

    reviewd=opinion.objects.filter(pk=opk).update(likecount=F("likecount") - 1)
    
    userObj = BookedUser.objects.get(authUser=request.user)
    item=Reviewlike.objects.filter(user=userObj, opinions=review)
    item.delete()
    dictV={}
    dictV['data']=review.likecount -1
    return JsonResponse(dictV)
    
    return HttpResponseRedirect('/books/'+str(pk)+'/')

def reviewDetail(request,pk):
    dictV = {}    
    book = get_object_or_404(Book, pk=pk)
    book.rating = round(book.rating * 2) / 2 #Round it to 0.5
    if book.rating == int(book.rating):
        book.rating = int(book.rating)
    try:
        coverPhoto = CoverPhoto.objects.filter(book=book)[0]
    except:
        coverPhoto = None
    dictV['conditions'] = conditionChoices #Conditions that a book can be in
    dictV['book'] = book
    dictV['coverPhoto'] = coverPhoto
    dictV['form'] = opinionForm()
    latest_review_list = opinion.objects.order_by('-pub_date')
    page = request.GET.get('page', 1)
    paginator = Paginator(latest_review_list, 10)
    try:
        pg = paginator.page(page)
    except PageNotAnInteger:
        pg = paginator.page(1)
    except EmptyPage:
        pg = paginator.page(paginator.num_pages)

    context = {'latest_review_list':latest_review_list}
    dictV['latest_review_list']=latest_review_list
    dictV['paginate_review_list']=pg
    likecount=Reviewlike.objects.values('opinions').annotate(num_like=Count('opinions'))
    dictV['likecount']=likecount
    return render(request,'reviewDetail.html',dictV)
@login_required        
def addReview(request, pk):
    book = get_object_or_404(Book, pk=pk)
    userObj = BookedUser.objects.get(authUser=request.user)
    form = opinionForm(request.POST)
    if form.is_valid():
        rating = form.cleaned_data['rating']
        comment = form.cleaned_data['comment']
        lrating = form.cleaned_data['lrating']
        crating = form.cleaned_data['crating']
        prating = form.cleaned_data['prating']
        wrating = form.cleaned_data['wrating']
        language = form.cleaned_data['language']
        cover = form.cleaned_data['cover']
        plot = form.cleaned_data['plot']
        writing = form.cleaned_data['writing']
    
        review = opinion()
        review.book = book
        review.user_name=userObj
        review.rating = rating
        review.lrating = lrating
        review.crating = crating
        review.prating = prating
        review.comment = comment
        review.language = language
        review.cover = cover
        review.plot = plot
        review.pub_date = datetime.datetime.now()
        review.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect('/books/'+str(pk)+'/')

    return render(request, 'bookDetails.html', {'book': book, 'form': form})

def addDiscussion(request, pk):
    book = get_object_or_404(Book, pk=pk)
    userObj = BookedUser.objects.get(authUser=request.user)
    form = discussionForm(request.POST)
    if form.is_valid():
        title = form.cleaned_data['title']
        comment = form.cleaned_data['comment']
        tags=form.cleaned_data['tags']
        #discusstags=form.cleaned_data['discusstags']
        discuss=Discuss()
        discuss.book_object=book
	discuss.user=userObj
        discuss.title=title
        discuss.comment=comment 
        #discuss.tags=tags


        #discuss.discusstags=discusstags
       # item=Activity.objects.create(actor=userObj, verb='Added a Discussion titled')    
           
        discuss.save()
        discuss.tags.add(*tags)
        print discuss.tags.slugs()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.

        return HttpResponseRedirect('/')

    return render(request, 'bookDetails.html', {'book': book, 'form': form})


def bookDiscussion(request,pk):
    dictV = {}
    try:
        userObj = BookedUser.objects.get(authUser=request.user)
    except:
        userObj = None
    book = Book.objects.get(pk=pk)
    book.rating = round(book.rating * 2) / 2 #Round it to 0.5
    if book.rating == int(book.rating):
        book.rating = int(book.rating)
    try:
        coverPhoto = CoverPhoto.objects.filter(book=book)[0]
    except:
        coverPhoto = None
    dictV['conditions'] = conditionChoices #Conditions that a book can be in
    dictV['book'] = book
    dictV['coverPhoto'] = coverPhoto
    sellingPrice = ( book.cost * (100-book.discount) )/ 100
    dictV['sellingPrice'] = sellingPrice
    addedToCart = request.GET.get('addedToCart')
    q=CartBook.objects.filter(book=dictV['book'])
    if q:
       
         incart=1;    
    else:
        incart=request.GET.get('incart')
    if request.user.is_authenticated():
        #check if the logged in user wants to lend a book or not and the book is not exchanged currently.
        b = possessedBooks.objects.filter(user=userObj,book=book,exchangedCurrently=False)
        if len(b):
            dictV['possessed'] = True
        else:
            dictV['possessed'] = False
    else:
        dictV['possessed'] = False

    #All lenders of a book
    people = [] #list of users who have not yet rented the book and possess it
    alreadyRequested = [] #Boolean list to check whether current book was already requested from a particular user or not
    q = possessedBooks.objects.filter(book=book,exchangedCurrently=False) #fetch possessedBooks objects for a particular book
    if userObj:
        q = q.exclude(user=userObj) #Show all users other than currently logged in user
    for x in q:
        people.append(x.user)
        exchangeObj = Exchange.objects.filter(book2=book,user1=userObj,user2=x.user)
        if len(exchangeObj):
            alreadyRequested.append(True)
        else:
            alreadyRequested.append(False)
    dictV['people'] = zip(people,alreadyRequested)

    dictV['addedToCart'] = addedToCart
    dictV['inCart']=incart
    return render(request,'bookDescription.html',dictV)   
@login_required
def likeDiscussion(request,pk,opk):    
    book = get_object_or_404(Book, pk=pk)
    discuss = get_object_or_404(Discuss, pk=opk)

    user = auth.models.User.objects.get(username=discuss.user.authUser)


    discussobj=Discuss.objects.filter(pk=opk).update(likecount=F("likecount") + 1)
    userObj = BookedUser.objects.get(authUser=request.user)
    item=Discusslike.objects.create(user=userObj, discuss=discuss)
    Activity.objects.create(actor=userObj, verb='Liked a discussion titled',action_object=discuss)    
         
    notify.send(userObj, recipient=user, target=discuss.user.authUser, verb='liked your discussion for the book'+book.title, action_object=discuss)   
    return HttpResponseRedirect('/books/'+str(pk)+'/')
@login_required
def commentdiscussion(request,pk,dpk):
         userObj = BookedUser.objects.get(authUser=request.user)
         discuss=Discuss.objects.get(pk=dpk)
         discussioncomment=DiscussionComment.objects.filter(discuss=discuss)
         replies=ReplyDcomment.objects.filter(reply=discussioncomment)
         #print replies
         dictV={}
         dictV['discuss']=discuss
         dictV['form']=discussioncommentForm()
         dictV['form1']=replydiscussioncommentForm()
         dictV['discussioncomment']=discussioncomment
         dictV['bid']=pk
         dictV['replies']=replies
         return render(request,'discussioncomments.html',dictV) 
@login_required
def adddiscussionComment(request,pk,dpk):
    userObj = BookedUser.objects.get(authUser=request.user)
    discuss=Discuss.objects.get(pk=dpk)
    discussioncomment=DiscussionComment.objects.filter(discuss=discuss)
    form = discussioncommentForm(request.POST)
    if form.is_valid():

        comment = form.cleaned_data['comment']
        
        discom = DiscussionComment()
        discom.discuss = discuss
        discom.comment = comment
        discom.user_name = userObj
        discom.pub_date = datetime.datetime.now()
         
        discom.save()
        return HttpResponseRedirect('/books/'+str(pk)+'/commentdiscussion/'+str(dpk)+'/')
    return render(request, 'discussioncomments.html', {'discuss': discuss, 'form': form})
@login_required
def replydiscussionComment(request,pk,opk,dpk):
    userObj = BookedUser.objects.get(authUser=request.user)
    discuss=DiscussionComment.objects.get(pk=dpk)
    
    form = replydiscussioncommentForm(request.POST)
    if form.is_valid():

        comment = form.cleaned_data['comment']
        
        discom = ReplyDcomment()
        discom.reply = discuss
        discom.comment = comment
        discom.user_name = userObj
        discom.pub_date = datetime.datetime.now()
        discom.save()
        return HttpResponseRedirect('/books/'+str(pk)+'/commentdiscussion/'+str(opk)+'/')
    return render(request, 'discussioncomments.html', {'discuss': discuss, 'form': form})
      
@login_required
def bookRemoveFromSubscriptionList(request,pk):
    userObj = BookedUser.objects.get(authUser=request.user)
    item = Subscriptionlist.objects.get(pk=pk)
    order=item.order
    item.delete()
    item2=Subscriptionlist.objects.filter(user=userObj,order__gte=order)
    for i in item2:
        i.order-=1
        i.save(update_fields=['order'])
    return HttpResponseRedirect('/books/subscription')

#To-Do: Disable up/down button for extreme entries
@login_required
def updatesubscription(request,pk):
        userObj = BookedUser.objects.get(authUser=request.user)
        item1=Subscriptionlist.objects.get(pk=pk)

        item2=Subscriptionlist.objects.get(order=item1.order+1, user=userObj)

        item1.order+=1
        item1.save(update_fields=['order'])
        print item2

        item2.order-=1
        item2.save(update_fields=['order'])
        return HttpResponseRedirect('/books/subscription')

@login_required
def follow(request,pk):
    user1= BookedUser.objects.get(pk=pk)
    user2=BookedUser.objects.get(authUser=request.user)
    item = Follow.objects.create(follower=user1,following=user2)
    user = auth.models.User.objects.get(username=user1.authUser)
    notify.send(request.user, recipient=user, target=user, verb='STARTED FOLLOWING YOU')   
    return HttpResponseRedirect('/books/')

@login_required
def sendMessage(request,pk,recieverId):
    dictV = {}
    dictV.update(csrf(request))
    userObj = get_object_or_404( BookedUser,pk=int(recieverId) )
    currentUser = get_object_or_404( BookedUser,authUser=request.user )
    form = msgForm(request.POST)
    dictV['form']=form
    dictV['spk']=pk
    dictV['rpk']=recieverId
        #If form is valid, save the exchange object
    if form.is_valid():
            data = request.POST.get('mg')
            #Create a message and send to user2
            messageObj = Message.objects.create(subject = 'Message',body=data,sender =currentUser.authUser ,recipient = userObj.authUser,sent_at = timezone.now() )
            dictV['Success'] = True
            return HttpResponseRedirect('/messages/outbox')
    else:
            print form.errors
            dictV['Failure'] = True
            dictV['error'] = form.errors

    return render(request,'msg.html',dictV)
@login_required
def feeds(request):
        dictV = []
        userObj = get_object_or_404( BookedUser,authUser=request.user )
        fol=Follow.objects.filter(following__authUser=request.user)
       # b=BookedUser.objects.filter(authUser__in=fol)
        feed1=Activity.objects.filter(actor__in=fol.values_list('following'))
        #fol1=BookedUser.objects.filter(authUser=fol.0.follower.authUser)
        #feed1=Activity.objects.filter(actor=Follow.objects.filter(following__authUser=request.user))
       # for f in fol:
        #    print f.following.authUser
         #   #foluser=BookedUser.objects.filter(authUser=f.following.authUser)
          #  feed=Activity.objects.filter(actor=f.following)
           # dictV.append(feed)
       # print dictV
        print feed1
        return HttpResponseRedirect('/books/')
