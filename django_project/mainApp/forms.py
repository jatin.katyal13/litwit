from django import forms
from django.contrib import auth
from models import *
from django.forms.extras.widgets import SelectDateWidget
from taggit.forms import TagWidget
from django_messages.models import Message
from taggit.managers import TaggableManager
from tagging.forms import TagField
from tagging_autocomplete_tagit.widgets import TagAutocompleteTagIt

class loginForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'Email','class': 'form-control',}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password','class': 'form-control',}),)
    

class bookedUserForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Name','class': 'form-control',}))
    penName = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Pen Name','class': 'form-control',}))
    phone = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Phone Number','class': 'form-control',}))
    dob = forms.DateField(widget=forms.TextInput(attrs={'class': 'datepicker sign_in',}))
    address = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','id':'locationTextField',}),required=False)
    latitude=forms.DecimalField(max_digits=9, decimal_places=6)
    longitude=forms.DecimalField(max_digits=9, decimal_places=6)
    favauthors = TagField(widget=TagAutocompleteTagIt(max_tags=5))
    favgenres = TagField(widget=TagAutocompleteTagIt(max_tags=5,attrs={'class':'ui-widget-content ui-autocomplete-input'}))
    profilePic = forms.ImageField(widget = forms.FileInput(attrs={ 'class': 'file' }),required=False )
    coverPic = forms.ImageField(widget = forms.FileInput(attrs={ 'class': 'file' }),required=False )
    class Meta:
        exclude = [ 'otp','authUser','canBorrow' ]
        model = BookedUser

class RegistrationForm(forms.ModelForm):
    #username = forms.RegexField(regex=r'^[\w.@+-]+$', max_length=30,widget=forms.TextInput(attrs={'placeholder': 'Username','class': 'form-control',} ) )
    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'Email','class': 'form-control',}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password','class': 'form-control',}),)
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Enter Password Again','class': 'form-control',}),)
    class Meta:
        model = auth.models.User
        fields = ('username','email','password','password1')

class exchangeForm(forms.ModelForm):
    message = forms.CharField(required=False, widget=forms.Textarea(attrs={'placeholder': 'Message','class': 'form-control','rows':'5','cols':'50'}))
    book1 = forms.ModelChoiceField(required=False, queryset=Book.objects.all(),widget=forms.Select(attrs={'class': 'form-control',}),)
    class Meta:
        fields = ( 'book1','message' )
        model = Exchange

class msgForm(forms.Form):
    mg = forms.CharField(required=True, widget=forms.Textarea(attrs={'placeholder': 'Message','class': 'form-control','rows':'5','cols':'50'}))

        

class contactForm(forms.ModelForm):
    phone = forms.IntegerField(required=False, widget=forms.TextInput(attrs={'type': 'number','class': 'form-control','min':'0'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control',}))
    query = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control','rows': '3','maxlength':'5000'}),required=False)
    class Meta:
        fields = ('phone','email','query')
        model = ContactUs

class opinionForm(forms.ModelForm):
   class Meta:
        model = opinion
        fields = ['rating', 'comment','lrating','language','crating','cover','prating','plot','wrating','writing']
        widgets = {
            'comment':forms.Textarea(attrs={'cols': 10, 'rows': 5}),
            'language':forms.Textarea(attrs={'cols': 30, 'rows': 8}),
            'cover':forms.Textarea(attrs={'cols': 30, 'rows': 8}),
            'plot':forms.Textarea(attrs={'cols': 30, 'rows': 8}),
            'writing':forms.Textarea(attrs={'cols': 30, 'rows': 8})
       
       
       
        }
class discussionForm(forms.ModelForm):
   class Meta:
        model = Discuss
        fields = ['title', 'comment','tags']
        widgets = {
            'comment':forms.Textarea(attrs={'cols': 10, 'rows': 3}),
            'title':forms.Textarea(attrs={'cols': 10, 'rows': 3}),
            
            
            
       
       
        }

class discussioncommentForm(forms.ModelForm):
   class Meta:
        model = DiscussionComment
        fields = ['comment']
        widgets = {
            
            'comment':forms.Textarea(attrs={'cols': 10, 'rows': 3}),
            
            
       
       
        }

class replydiscussioncommentForm(forms.ModelForm):
   class Meta:
        model = ReplyDcomment
        fields = ['comment']
        widgets = {
            
            'comment':forms.Textarea(attrs={'cols': 10, 'rows': 3}),
            
            
       
       
        }

