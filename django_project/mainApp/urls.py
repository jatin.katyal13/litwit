"""booked URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from mainApp import views
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.indexView),
    url(r'book/(?P<pk>\d*)/$', views.bookDetails),
    url(r'books/(?P<pk>\d*)/lendIt/', views.lendIt), #Put up a book for lending
    url(r'series/(?P<pk>\d*)/$', views.seriesDetails),
    url(r'^book/(?P<pk>\d*)/addToWishlist/$', views.bookAddToWishlistBook),
    url(r'^book/(?P<pk>\d*)/addToreadList/$', views.bookAddToReadList),
    url(r'^book/(?P<pk>\d*)/addToCart/$', views.bookAddToCart),
    url(r'^book/(?P<pk>\d*)/addToRentalCart/$', views.bookAddToRentalCart),
    url(r'^books/(?P<pk>\d*)/subscribecart/$', views.bookAddTosubscriptionlist),
    url(r'^book/cart/(?P<pk>\d*)/removeFromBookCart/$', views.bookRemoveFromCart),
    url(r'^book/cart/(?P<pk>\d*)/removeFromRentalCart/$', views.bookRemoveFromRentalCart),
    url(r'^book/cart/(?P<pk>\d*)/removeFromSeriesCart/$', views.seriesRemoveFromCart),
    url(r'^book/cart/$', views.bookCart),
    url(r'^address/$',views.shipmentaddress),
    url(r'^payment/(?P<apk>\d*)$',views.orderconfirmation),
    url(r'^subscriptiongateway/(?P<apk>\d*)$',views.subscriptionconfirmation),
    url(r'^reviewOrder/(?P<apk>\d*)$',views.reviewOrder),
    url(r'^sendOtp/$', views.sendOtp),
    url(r'^verifyOtp/$', views.verifyOtp),
    url(r'^codsubscription/$', views.codsubscription),
    url(r'^subscribe/$', views.subscriptionplan),
    url(r'^subscription/cart/$', views.subscriptiondisplay),
    url(r'^subscription/cart/delieverd/(?P<pk>\d*)/$', views.subscriptiondeliver),
    url(r'^addressconfirmation/$',views.subscriptionaddress),
    url(r'^reviewOrderSubscription/(?P<apk>\d*)$',views.reviewSubscription),
    url(r'^subscriptionpayment/(?P<apk>\d*)$',views.subscriptionorderconfirmation),
    #User Profile
    url(r'^accounts/profile/$', views.viewProfile),
    url(r'^accounts/editProfile/$', views.editProfile),
    #url(r'^accounts/login/$', views.login), #added facebook and google login, so using django-allauth login now
    #url(r'^accounts/logout/$', views.logout), #added facebook and google login, so using django-allauth logout now
    url(r'^accounts/register/$', views.register), #added facebook and google login, so using django-allauth registration now
    url(r'^accounts/', include('allauth.urls')),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
